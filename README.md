# DJANGO WHO ICD MODULE

## Описание 

Содежит django проект c модулем **who_icd**

Модуль who_icd - содержит парсер данных [WHO ICD api](https://icd.who.int/icdapi)
и django модели

## Requirements

необходимы такие пакеты

```
jmespath
django-modeltranslation
django-nested-admin
django-admin-sortable2
ckeditor
```

добавить в **INSTALL_APPS**

```
'apps.who_icd',
'apps.who_icd.contrib.parser',
```


## Предподготовка

Необходимо в код проекта внедрить запуск docker контейнера образа **whoicd/icd-api**

[doc образа](https://icd.who.int/docs/icd-api/ICDAPI-DockerContainer/)

именно из него парсер будет получать данные


## Как запустить парсер

Необходимо создать экземпляр модели ParseQueueSession
и используя кнопки интерфейса запустить процесс парсинга


### ВАЖНО !

Контейнер **whoicd/icd-api** запускаеться пару минут


### Дополнительная информация

В конфигурации этого проекта, при запущеном контейнере icd можно зайти в сваггер

http://localhost:9000/swagger/index.html

другие ссылки

http://localhost:9000/browse

http://localhost:9000/ct
