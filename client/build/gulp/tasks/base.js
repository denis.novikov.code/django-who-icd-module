import path from 'path'
import gulp from 'gulp'
const chalk = require('chalk')

var options = {
  project: 'app-' + getDefaultContext('canonium')
}

function runInContext(filepath, cb) {
  var context = path.relative(process.cwd(), filepath);
  var projectName = context.split(path.sep)[0];

  // Console
  console.log(
    '[' + chalk.green(projectName.replace('app-', '')) + ']' +
    ' has been changed: ' + chalk.cyan(context)
  );

  // Set project
  options.project = projectName;
  cb()
}

function getDefaultContext(defaultName) {
  var argv = process.argv[2] || process.argv[3];
  if (typeof argv !== 'undefined' && argv.indexOf('--') < 0) {
    argv = process.argv[3];
  }
  return (typeof argv === 'undefined') ? defaultName : argv.replace('--', '');
}

export default class Task {
  constructor(config) {
    this.config = config
  }

  binded(func) {
    return this[func].bind(this)
  }

  watch(task) {
    gulp.watch(this.config.glob).on('change', function(filepath) {
      runInContext(filepath, gulp.series(task)) })
  }
}
