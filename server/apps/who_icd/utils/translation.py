from django.conf import settings
from django.utils.translation import get_language, override

__all__ = ('trans_field', 'base_trans_field')


def trans_field(field_name):
    return [f'{field_name}_{code}' for code, lable in settings.LANGUAGES]


def field_trans_name(field_name, language):
    return f'{field_name}_{language.lower()}'


def force_get_trans_field(obj, field_name, default=None):
    code = get_language()
    field_name_lang = f'{field_name}_{code}'
    return getattr(obj, field_name_lang, getattr(obj, field_name, default))


def base_trans_field(field_name):
    return f'{field_name}_{settings.LANGUAGE_CODE}'


def helper_get_translated_widgets(field_names, widget):
    widgets = {}

    for name in field_names:
        for key, value in settings.LANGUAGES:
            widgets["{}_{}".format(name, key)] = widget

    return widgets


def get_translation_in(language, gettext_string):
    with override(language):
        return str(gettext_string)