from functools import lru_cache

from django.urls import reverse
from django.utils.safestring import mark_safe


def admin_change_url(obj):
    """
    Return admin change url for given object
    """
    app_label = obj._meta.app_label
    model_name = obj._meta.model.__name__.lower()
    return reverse(f'admin:{app_label}_{model_name}_change', args=(obj.pk,))


def admin_add_url(obj):
    """
    Return admin change url for given object
    """
    app_label = obj._meta.app_label
    model_name = obj._meta.model.__name__.lower()
    return reverse(f'admin:{app_label}_{model_name}_add', args=(obj.pk,))


@lru_cache(maxsize=16)
def admin_changelist_url(model):
    """
    Return admin change list url fir given model
    """
    app_label = model._meta.app_label
    model_name = model.__name__.lower()
    return reverse(f'admin:{app_label}_{model_name}_changelist')


def admin_object_link(obj):
    return mark_safe(f'<a href="{admin_change_url(obj)}">{obj}</a>')