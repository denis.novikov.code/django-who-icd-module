

def cycle_getattr(obj, path, default=None):
    result = obj

    steps = path.split('.')

    try:
        for step in steps:
            result = getattr(result, step)

    except AttributeError:
        result = default

    return result


def cycle_getitem(obj, path, default=None):
    result = obj

    steps = path.split('.')

    try:
        for step in steps:
            result = result[step]

    except (KeyError, TypeError):
        result = default

    return result