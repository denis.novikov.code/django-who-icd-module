import re
from datetime import datetime

from django.db import transaction

from apps.who_icd.contrib.parser.models import QueueElement
from apps.who_icd.contrib.parser.utils import cut_uri, extend_entity_url, is_entity
from apps.who_icd.models import Release, Entity
from apps.who_icd.selectors import get_entity
from apps.who_icd.utils.cycle_getattr import cycle_getitem
from apps.who_icd.utils.translation import field_trans_name


class BaseTypedDataSaver:
    model = None

    @classmethod
    def save(cls, queue_elem, data):
        ...

    @classmethod
    def create_next_parser_queue_elems(cls, queue_elem, data):
        ...


class LinearizationDataHandler(BaseTypedDataSaver):

    @classmethod
    def create_next_parser_queue_elems(cls, queue_elem, data):

        release_urls = data['release']

        queue_elements = []

        for release_url in release_urls[:1]:
            queue_elements.append(QueueElement(
                parse_session_id=queue_elem.parse_session.pk,
                language=queue_elem.language,
                url=cut_uri(release_url),
        ))

        QueueElement.objects.bulk_create(queue_elements, ignore_conflicts=True)


class ReleaseDataHandler(BaseTypedDataSaver):

    model = Release

    @classmethod
    def save(cls, queue_elem, data):
        cls.model.objects.update_or_create(
            icd_id=cut_uri(data['@id']),
            defaults={
                'release_id': data['releaseId'],
                field_trans_name('title', queue_elem.language): data['title']['@value'],
                'available_languages': data['availableLanguages'],
                'date': datetime.strptime(data['releaseDate'], '%Y-%m-%d'),
                'metadata': data
            }
        )

    @classmethod
    def create_next_parser_queue_elems(cls, queue_elem, data):

        child_urls = data.get('child', [])

        queue_elements = []

        for child_url in child_urls:
            queue_elements.append(QueueElement(
                parse_session_id=queue_elem.parse_session.pk,
                language=queue_elem.language,
                url=cut_uri(extend_entity_url(child_url)),
        ))

        QueueElement.objects.bulk_create(queue_elements, ignore_conflicts=True)


class EntityDataHandler(BaseTypedDataSaver):

    model = Entity

    @classmethod
    def save(cls, queue_elem, data):

        parent_icd_id = cut_uri(extend_entity_url(data['parent'][0]))
        parent = None if not is_entity(parent_icd_id) else get_entity(parent_icd_id)
        parent_depth = 0 if not parent else parent.depth

        transaction.on_commit(lambda: cls.model.objects.update_or_create(
            icd_id=cut_uri(extend_entity_url(data['@id'])),
            defaults={
                'code': data['code'],
                field_trans_name('title', queue_elem.language): data['title']['@value'],
                field_trans_name('definition', queue_elem.language): cycle_getitem(data, 'definition.@value', ''),
                field_trans_name('specified_name', queue_elem.language): cycle_getitem(data, 'fullySpecifiedName.@value', ''),
                'class_kind': data['classKind'],
                'parent': parent,
                # 'parent_id': None if not is_entity(parent) else cut_uri(extend_entity_url(parent)),
                'depth': parent_depth + 1,
                field_trans_name('metadata', queue_elem.language): data,
            }
        ))

    @classmethod
    def create_next_parser_queue_elems(cls, queue_elem, data):
        # exclusion = [ref for itm in data.get('exclusion', []) if (ref := itm['linearizationReference'])]
        # foundation_child_elsewhere = [ref for itm in data.get('foundationChildElsewhere', []) if (ref := itm['linearizationReference'])]

        child_urls = data.get('child', [])# + data.get('descendant', []) + exclusion + foundation_child_elsewhere

        queue_elements = []

        for child_url in child_urls:
            queue_elements.append(QueueElement(
                parse_session_id=queue_elem.parse_session.pk,
                language=queue_elem.language,
                url=cut_uri(extend_entity_url(child_url)),
        ))

        QueueElement.objects.bulk_create(queue_elements, ignore_conflicts=True)


HANDLERS = {
    r'/release/\d{2}/\w*$': LinearizationDataHandler,
    r'/release/\d{2}/\d{4}-\d{2}/\w*$': ReleaseDataHandler,
    r'/release/\d{2}/\d{4}-\d{2}/\w*/\d*(\?include=[a-zA-Z,]*)?$': EntityDataHandler,
    r'/release/\d{2}/\d{4}-\d{2}/\w*/\d*/\w*(\?include=[a-zA-Z,]*)?$': EntityDataHandler,
    # 'icd/release/\d{2}/\d{4}-\d{2}/\w*/\d*(/\w*)?(\?include=[a-zA-Z,]*)?$': 'get_entity',
}


def get_data_handler(elem):

    url = elem.url

    for pattern, handler in HANDLERS.items():
        is_match = bool(re.match(pattern, url))

        if is_match:
            return handler