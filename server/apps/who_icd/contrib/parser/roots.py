import re
from .sdk import icd_sdk

ROOTS = {
    r'/release/\d{2}/\w*$': 'get_data',
    r'/release/\d{2}/\d{4}-\d{2}/\w*$': 'get_data',
    r'/release/\d{2}/\d{4}-\d{2}/\w*/\d*(\?include=[a-zA-Z,]*)?$': 'get_data',
    r'/release/\d{2}/\d{4}-\d{2}/\w*/\d*/\w*(\?include=[a-zA-Z,]*)?$': 'get_data',
    # 'icd/release/\d{2}/\d{4}-\d{2}/\w*/\d*(/\w*)?(\?include=[a-zA-Z,]*)?$': 'get_entity',
}


def get_parser_method_name(elem):

    url = elem.url

    for pattern, method_name in ROOTS.items():
        is_match = bool(re.match(pattern, url))

        if is_match:
            return method_name


