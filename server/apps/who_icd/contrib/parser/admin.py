from django.contrib import admin
from django.http import HttpResponseRedirect
from django.utils.safestring import mark_safe
from django.utils.translation import pgettext_lazy

from .services import icd_api_is_available
from .tasks import start_parsing_task, continue_parsing_task
from . import models


@admin.register(models.ParseQueueSession)
class ParseSessionModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'status')
    fields = ['id', 'status', 'created_at', 'start_parsing', 'continue_parsing', 'service_is_running']
    readonly_fields = ['id', 'created_at', 'start_parsing', 'continue_parsing', 'service_is_running']

    def start_parsing(self, obj):

        if obj.pk and obj.status == obj.Statuses.NEW:
            msg = pgettext_lazy("who_icd", "Start")

            return mark_safe(
                f'<input type="submit" '
                f'value={msg} '
                f'name="_start_parsing" />'
            )

        return '-'

    start_parsing.short_description = (
        pgettext_lazy("who_icd", "Start parsing")
    )

    def continue_parsing(self, obj):

        if obj.pk and obj.status == obj.Statuses.STARTED:
            msg = pgettext_lazy("who_icd", "Continue")

            return mark_safe(
                f'<input type="submit" '
                f'value={msg} '
                f'name="_continue_parsing" />'
            )

        return '-'

    continue_parsing.short_description = (
        pgettext_lazy("who_icd", "Continue parsing")
    )

    def response_change(self, request, obj):
        if "_start_parsing" in request.POST:
            start_parsing_task.delay(obj.pk)
            self.message_user(
                request,
                pgettext_lazy(
                    "who_icd",
                    '`start_parsing_task` task run'
                ),
            )
            return HttpResponseRedirect(".")

        elif "_continue_parsing" in request.POST:
            continue_parsing_task.delay()
            self.message_user(
                request,
                pgettext_lazy(
                    "who_icd",
                    '`parse_next_elem_task` task run'
                ),
            )
            return HttpResponseRedirect(".")

        return super().response_change(request, obj)

    def service_is_running(self, obj):
        return 'running...' if icd_api_is_available() else 'stopped'


@admin.register(models.QueueElement)
class QueueElementModelAdmin(admin.ModelAdmin):
    list_filter = ('parse_session', 'is_done', 'language')
    list_display = ('url', 'language',  'is_done', 'created_at')


@admin.register(models.ParseErrorLog)
class ParseErrorLogModelAdmin(admin.ModelAdmin):
    list_display = ('url', 'exception', 'message', 'traceback', 'created_at')
