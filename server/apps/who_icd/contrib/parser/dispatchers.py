from django.db.models.signals import post_save
from django.dispatch import receiver


@receiver(post_save, sender='icd_parser.ParseQueueSession')
def update_queue_elements(sender, instance, **kwargs):

    tracker = instance.field_tracker
    if tracker.has_changed('status') and instance.status == sender.Statuses.DONE:
        instance.elements.filter(is_done=False).update(is_done=True)