import requests

from .client import ICDClient
from .requests.adapter import BaseServiceAdapter


class WhoIcdApiServiceAdapter(BaseServiceAdapter):
    version = 'v2'
    service_title = 'WHO ICD api'
    service_type = 'outgoing'
    request_client_class = ICDClient

    def get_data(self, called_method, language):
        return self.call(called_method, request_method='get', headers={'Accept-Language': language})

    def is_running(self, except_error=True):

        try:
            response = self.call(
                '/release/11/mms/',
                request_method='get',
                headers={'Accept-Language': 'en'},
                full_response=True
            )

        except requests.ConnectionError as e:

            if except_error:
                return False

            else:
                return False, e

        return response.code == 200, None


icd_sdk = WhoIcdApiServiceAdapter()