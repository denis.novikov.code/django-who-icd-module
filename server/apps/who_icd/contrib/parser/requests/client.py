import time
from abc import ABC, abstractmethod
from typing import Dict, Union, Tuple

from .auth import AbstractRequestClientAuth, RequestClientNoAuth
from .details import rebuild_response

__all__ = (
    'BaseRequestClient',
    'RequestClient',
    'RequestAuthClientMixin',
    'RequestLoggedClientMixin',
    'RequestLoggedAuthClient',
)


class BaseRequestClient(ABC):
    """
    Base class to work with remote API server. Use this one in your
    APIClient's variants.
    """

    def __init__(self, api_url: str, http_client):
        """
        Initializes api connector. Received parameters required to
        connect remote server.

        Args:
            api_url (str, optional): Base url used to construct url.
        """
        self.api_url = api_url
        self.http_client = http_client

    def construct_url(self, *args) -> str:
        """
        Returns url with joined args as parts of url.

        Args:
            *args: part of url.

        Returns:
            str: URL
        """
        url = self.api_url

        if not args:
            return url

        joined_args = '/'.join([x.lstrip('/') for x in args])

        return f'{url}{joined_args}'

    def get_base_headers(self) -> Dict:
        """
        Return dictionary of HTTP Headers to send with your request
        """
        return {'Content-Type': 'application/json'}

    @abstractmethod
    def make_request(self, **kwargs) -> 'Response':
        raise NotImplementedError


class RequestClient(BaseRequestClient):

    def make_request(
            self,
            method: str,
            path: str = '',
            params: Dict = None,
            data: Dict = None,
            json: Dict = None,
            files: Dict = None,
            headers: Dict = None,
            timeout: int = 30,
            **kwargs
    ) -> 'Response':

        url = self.construct_url(path)

        if headers is None:
            headers = {}

        headers = self.get_headers(headers)

        return self.request_process(
            method=method,
            url=url,
            params=params,
            data=data,
            json=json,
            files=files,
            headers=headers,
            timeout=timeout,
            **kwargs
        )

    def get_headers(self, headers: Union[Dict, None]):

        if headers is None:
            headers = {}

        base_headers = self.get_base_headers()

        base_headers.update(headers)

        return headers

    def request_process(self, *args, **kwargs):

        args, kwargs = self.before_request(*args, **kwargs)

        response = self.request(*args, **kwargs)

        self.after_request(response, *args, **kwargs)

        return response

    def before_request(self, *args, **kwargs) -> Tuple:
        return args, kwargs

    def request(
        self,
        method: str,
        url: str,
        params: Dict = None,
        data: Dict = None,
        json: Dict = None,
        files: Dict = None,
        headers: Dict = None,
        timeout: int = 20,
        auth=None,
        **kwargs,
    ):

        request_response = self.http_client(
                                url=url,
                                method=method,
                                params=params,
                                data=data,
                                json=json,
                                files=files,
                                headers=headers,
                                timeout=timeout,
                                auth=auth
                            )

        response = rebuild_response(request_response)
        return response

    def after_request(self, response, *args, **kwargs):
        ...


class RequestAuthClientMixin:

    pause_after_add_auth = 0

    def add_authorization(self, *args, request_client_auth: AbstractRequestClientAuth = RequestClientNoAuth(), **kwargs):
        request_client_auth.update_headers(**kwargs)
        request_client_auth.update_body(**kwargs)
        kwargs['url'] = request_client_auth.update_query(**kwargs)
        kwargs['auth'] = request_client_auth.auth(**kwargs)
        return args, kwargs

    def request_process(self, *args, **kwargs):

        args, kwargs = self.before_request(*args, **kwargs)
        args, kwargs = self.add_authorization(*args, **kwargs)

        time.sleep(self.pause_after_add_auth)
        response = self.request(*args, **kwargs)

        self.after_request(response, *args, **kwargs)

        return response

    def make_request(
            self,
            *args,
            request_client_auth,
            **kwargs
    ) -> 'Response':

        return super().make_request(*args, request_client_auth=request_client_auth, **kwargs)


class RequestLoggedClientMixin:

    def request(self, *args, **kwargs):

        response = super().request(*args, **kwargs)
        self.log(response, kwargs)

        return response

    def log(self, response, kwargs):
        ...


class RequestLoggedAuthClient(RequestAuthClientMixin, RequestLoggedClientMixin, RequestClient):
    ...