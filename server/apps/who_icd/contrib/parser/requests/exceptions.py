import json
import traceback

get_traceback = lambda e: {
                            'exception': str(type(e).__name__),
                            'message': str(e),
                            'traceback': traceback.format_exc(),
                        }

get_json_traceback = lambda e: json.dumps(get_traceback(e))