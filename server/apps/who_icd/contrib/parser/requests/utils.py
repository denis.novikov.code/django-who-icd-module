

def clean_dict(data):
    if data is None:
        return {}

    return {
        key: value for key, value in data.items()
        if value is not None
    }