import ast
import json
from typing import AnyStr, NamedTuple, Dict, Union

from requests import PreparedRequest
from requests.utils import guess_json_utf

Response = NamedTuple('Response', [
    ('code', int),
    ('url', str),
    ('request', Dict),
    ('response', Dict),
    ('body', Union[Dict, str])
])


def string_to_dict(value: AnyStr, encoding=None):

    if isinstance(value, bytes) and encoding:

        if not encoding:
            encoding = guess_json_utf(value)

        value = value.decode(encoding)

    try:
        value = json.loads(value)

    except Exception:
        value = ast.literal_eval(value)

    if not isinstance(value, dict):
        raise ValueError('Cant transform value to dict')

    return value


def get_response_body(response: Response):
    try:
        body = response.json()

    except Exception:
        body = response.text

        try:
            body = string_to_dict(body, response.encoding)

        except Exception:
            ...

    return body


def get_request_body(request: PreparedRequest):
    body = request.body

    if not body:
        body = {}

    elif isinstance(body, bytes):
        body = body.decode('utf-8')

        try:
            body = json.loads(body)

        except Exception:
            body = {'raw': body}

    return body


def get_response_history(response: Response):
    response_data = dict(
        url=response.url,
        reason=response.reason,
        status_code=response.status_code,
        encoding=response.encoding,
        headers=dict(response.headers),
    )

    data = [response_data]

    if response.history:
        for resp in response.history:
            data.extend(get_response_history(resp))

    return data


def get_request_details(request: PreparedRequest):

    data = dict()

    data['method'] = request.method
    data['url'] = request.url
    data['headers'] = dict(request.headers)
    data['body'] = get_request_body(request)

    return data


def get_response_details(response: Response):
    data = dict()

    data['url'] = response.url
    data['reason'] = response.reason
    data['status_code'] = response.status_code
    data['elapsed'] = response.elapsed.total_seconds()
    data['encoding'] = response.encoding
    data['headers'] = dict(response.headers)
    data['body'] = get_response_body(response)
    data['history'] = get_response_history(response)
    data['request'] = get_request_details(response.request)

    return data


rebuild_response = lambda response: Response(
    url=response.url,
    code=response.status_code,
    request=get_request_details(response.request),
    response=get_response_details(response),
    body=get_response_body(response),
)