from abc import ABC, abstractclassmethod, abstractmethod
from typing import Dict, Optional
from urllib import parse
from django.http.request import QueryDict
from requests.auth import HTTPBasicAuth, AuthBase


class AbstractRequestClientAuth(ABC):

    def __init__(self, *args, **kwargs):
        ...

    def auth_header(self, **kwargs) -> Dict:
        return {}

    def auth_body(self, **kwargs) -> Dict:
        return {}

    def auth_query(self, **kwargs) -> Dict:
        return {}

    def auth(self, **kwargs) -> Optional['AuthBase']:
        return None

    def update_headers(self, headers, **kwargs):
        auth_headers = self.auth_header(**kwargs)
        headers.update(auth_headers)

    def update_body(self, **kwargs):
        auth_body = self.auth_body(**kwargs)

        if kwargs.get('data'):
            kwargs['data'].update(auth_body)

        elif kwargs.get('json'):
            kwargs['json'].update(auth_body)

    def update_query(self, url, **kwargs) -> str:

        auth_query = self.auth_query(**kwargs)

        QUERY_INDEX = 3
        components = list(parse.urlsplit(url))

        query_dict = QueryDict(components[QUERY_INDEX], mutable=True)
        query_dict.update(auth_query)

        components[QUERY_INDEX] = query_dict.urlencode()

        updated_query = parse.urlunsplit(components)
        return updated_query


class RequestClientNoAuth(AbstractRequestClientAuth):
    ...


class BasicAuthorizationRequestClientAuth(AbstractRequestClientAuth):

    def __init__(self, *, username, password):
        self.username = username
        self.password = password

    def auth(self, **kwargs) -> Optional['AuthBase']:
        return HTTPBasicAuth(username=self.username, password=self.password)