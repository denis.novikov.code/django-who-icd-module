import random
import time

import requests
from requests.adapters import HTTPAdapter
from typing import Dict, Optional, Union
from urllib3 import Retry


def get_random_user_agent():
    USER_AGENTS = [
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_8_3 rv:6.0; EO) AppleWebKit/532.0.1 (KHTML, like Gecko) Version/6.1.7 Safari/532.0.1",
        "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/5.0; .NET CLR 3.4.29054.4)",
        "Mozilla/5.0 (Windows; U; Windows NT 5.0) AppleWebKit/535.1.0 (KHTML, like Gecko) Chrome/29.0.872.0 Safari/535.1.0",
        "Mozilla/5.0 (Windows; U; Windows NT 5.3) AppleWebKit/531.1.2 (KHTML, like Gecko) Chrome/16.0.819.0 Safari/531.1.2",
        "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 5.1; Trident/7.0; .NET CLR 1.3.29754.8)",
        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:13.9) Gecko/20100101 Firefox/13.9.4",
        "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_10_1 rv:6.0; EU) AppleWebKit/534.2.1 (KHTML, like Gecko) Version/5.0.7 Safari/534.2.1",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1) AppleWebKit/535.0.2 (KHTML, like Gecko) Chrome/21.0.876.0 Safari/535.0.2",
        "Mozilla/5.0 (Windows; U; Windows NT 6.2) AppleWebKit/531.0.2 (KHTML, like Gecko) Chrome/36.0.804.0 Safari/531.0.2",
        "Mozilla/5.0 (Windows; U; Windows NT 5.2) AppleWebKit/537.0.0 (KHTML, like Gecko) Chrome/28.0.838.0 Safari/537.0.0",
    ]
    return random.choice(USER_AGENTS)


def get_proxies():
    proxies = {
        # 'http': 'http://Selkatuxadmitr:G9t9VyC@2.59.60.46:45785',  # free vpn
        # 'https': 'http://Selkatuxadmitr:G9t9VyC@2.59.60.46:45785',
        # 'http': 'http://oshekaroman:PUCgYWAryA@216.185.47.87:50100',
        # 'https': 'http://oshekaroman:PUCgYWAryA@216.185.47.87:50100',
    }
    return proxies


def requests_retry_session(
    retries=50,
    backoff_factor=0.2,
    status_forcelist=(500, 502, 504),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


def retry_request(
        url: str,
        method: str = 'get',
        params: Optional[Union[Dict, str]] = None,
        data: Optional[Union[Dict, str]] = None,
        json: Dict = None,
        files: Dict = None,
        headers: Optional[Dict] = None,
        timeout: int = 20,
        auth=None,
        raise_for_status: bool = False,
        use_proxy: bool = False,
        use_random_user_agent: bool = True,
        restarts: int = 100,
        retry_session_params: Optional[dict] = None,
        before_request_pause: Optional[int] = 0
):
    loops = iter(range(1, 1 + restarts))
    success = False
    response = None

    if use_proxy:
        proxies = get_proxies()
    else:
        proxies = ()

    if headers is None:
        headers = {}

    while not success and next(loops, None):

        if retry_session_params:
            retry_session = requests_retry_session(**retry_session_params)
        else:
            retry_session = requests_retry_session()

        request_method = getattr(retry_session, method, None)

        if not request_method:
            raise AttributeError(f'{method} is not supported')

        if use_random_user_agent:
            headers['User-Agent'] = get_random_user_agent()

        # Delete method accepts only path, without extra params
        request_params = dict(
            url=url,
            params=params,
            data=data,
            json=json,
            files=files,
            headers=headers,
            timeout=timeout,
            auth=auth,
            proxies=proxies,
        )

        time.sleep(before_request_pause)

        response = request_method(**request_params)

        try:
            data = response.json()

        except Exception as e:
            print(e)
            ... # TODO log it

        else:
            success = True

    if raise_for_status:
        response.raise_for_status()

    return response


def alt_retry_request(
        *,
        url,
        method,
        params,
        data,
        json,
        files,
        headers,
        timeout,
        auth
):

    request_method = getattr(requests, method)

    for sleep_time in [3, 60, 60 * 10]:

        try:
            return request_method(
                    url=url,
                    params=params, data=data,
                    json=json, files=files,
                    headers=headers, timeout=timeout,
                    auth=auth
                )

        except OSError as e:
            error = e
            time.sleep(sleep_time)

    raise error