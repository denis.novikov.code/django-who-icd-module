import jmespath
from abc import ABC
from typing import Dict, Optional, Type, Union, Iterator, List, Callable, Iterable

from .client import RequestLoggedAuthClient, AbstractRequestClientAuth, RequestClientNoAuth
from .utils import clean_dict


class AbstractServiceAdapter(ABC):

    version = 'v1'

    def get_request_client(self, context):
        raise NotImplementedError

    def get_client_authorization(self, context):
        raise NotImplementedError


class BaseServiceAdapter(AbstractServiceAdapter):

    version = 'v1'
    service_title = None
    service_type = 'outgoing'

    errors_path = 'data.error'
    key_transformer: Callable = None

    request_client_class = None
    client_authorization_class = None

    def __init__(
            self,
            request_client_class: 'RequestLoggedAuthClient' = None,
            client_authorization_class: 'AbstractRequestClientAuth' = RequestClientNoAuth,
    ):

        if request_client_class:
            self.request_client_class = request_client_class

        if client_authorization_class:
            self.client_authorization_class = client_authorization_class

    def get_request_client(self, context):
        return self.request_client_class()

    def get_client_authorization(self, context):
        return self.client_authorization_class()

    def check_errors(self, response, response_json: Dict, success_codes: List):

        if success_codes and response.code not in success_codes:

            data = dict(code=response.code)

            if self.errors_path:
                errors = jmespath.search(self.errors_path, response_json)
                data['errors'] = errors

            self.raise_exception(data)

    def raise_exception(self, data):
        raise Exception(data)

    @staticmethod
    def _generate_entities_from_data(
            entities_data: Iterable,
            entity_class: Optional[Type['BaseImportEntity']] = None,
    ):

        return (entity_class(**entity_data) for entity_data in entities_data)


    @staticmethod
    def _get_entities_data(
            response_data: List,
            entity_path: str = None,
            key_transformer: Callable = None,
    ) -> Iterable:

        items_data = response_data

        if entity_path:

            items_data = (jmespath.search(entity_path, item) for item in items_data)

        # items_data_raw = [{**item} for item in items_data]

        if key_transformer:
            items_data = [key_transformer(item) for item in items_data]

        # for item, raw_item in zip(items_data, items_data_raw):
        #     item['raw_data'] = raw_item

        return items_data

    def service_request(
            self,
            called_method: str,
            request_method: str = 'post',
            params: Optional[Dict] = None,
            data: Optional[Dict] = None,
            json: Optional[Dict] = None,
            timeout: int = 20,
            success_codes=None,
            **kwargs,
    ):

        request_client = self.get_request_client(kwargs)
        request_client_auth = self.get_client_authorization(kwargs)

        response: 'Response' = request_client.make_request(
            method=request_method,
            params=clean_dict(params),
            data=data,
            json=json,
            request_client_auth=request_client_auth,
            path=called_method,
            timeout=timeout,
            **kwargs,
        )

        response_data: Union[Dict, str] = response.body

        self.check_errors(response, response_data, success_codes)

        return response, response_data

    def get_service_method_request_result(
            self,
            called_method: str,
            entity_class: Optional[Type['BaseImportEntity']] = None,
            params: Optional[Dict] = None,
            data: Optional[Dict] = None,
            entity_path: str = None,
            items_path: str = None,
            is_single: bool = False,
            is_json: bool = True,
            request_method: str = 'post',
            timeout: int = 20,
            full_response: bool = False,
            success_codes=None,
            **kwargs
    ) -> Union['BaseImportEntityVar', Iterator['BaseImportEntityVar']]:

        response, response_data = self.service_request(
            called_method=called_method,
            params=params,
            data=None if is_json else data,
            json=None if not is_json else data,
            request_method=request_method,
            timeout=timeout,
            success_codes=success_codes,
            **kwargs,
        )

        if full_response:
            data = [response]

        elif isinstance(response_data, dict):
            necessary_data = self.get_necessary_data(response_data, items_path)

            entities_data = self._get_entities_data(
                response_data=necessary_data,
                entity_path=entity_path,
                key_transformer=self.key_transformer
            )

            if entity_class:
                data = self._generate_entities_from_data(
                    entities_data=entities_data,
                    entity_class=entity_class,
                )

            else:
                data = entities_data

        else:
            data = [response_data]

        return iter(data)

    def get_necessary_data(self, response_json, items_path):
        return [response_json] if not items_path else jmespath.search(items_path, response_json)

    def call(
            self,
            called_method: str = '',
            entity_class: Optional[Type['BaseImportEntity']] = None,
            params: Optional[Dict] = None,
            data: Optional[Dict] = None,
            entity_path: str = None,
            is_json: bool = True,
            request_method: str = 'post',
            timeout: int = 200,
            full_response: bool = False,
            success_codes=None,
            **kwargs
    ):

        return next(self.get_service_method_request_result(
            called_method=called_method,
            entity_class=entity_class,
            params=params,
            data=data,
            entity_path=entity_path,
            is_single=True,
            is_json=is_json,
            request_method=request_method,
            timeout=timeout,
            service_type=self.service_type,
            service_title=self.service_title,
            full_response=full_response,
            success_codes=success_codes,
            **kwargs,
        ), None)

    def call_list(
            self,
            called_method: str = '',
            entity_class: Optional[Type['BaseImportEntity']] = None,
            params: Optional[Dict] = None,
            data: Optional[Dict] = None,
            entity_path: str = None,
            items_path: str = None,
            is_json: bool = True,
            request_method: str = 'post',
            # limit: int = None,
            # offset: int = None,
            timeout: int = 200,
            full_response: bool = False,
            success_codes=None,
            **kwargs
    ):

        params = params or dict()

        paginated, params = self.add_paginated_list_params(params, kwargs)

        while True:
            data: Iterator = self.get_service_method_request_result(
                called_method=called_method,
                entity_class=entity_class,
                params=params,
                data=data,
                entity_path=entity_path,
                items_path=items_path,
                is_json=is_json,
                request_method=request_method,
                timeout=timeout,
                service_type=self.service_type,
                service_title=self.service_title,
                full_response=full_response,
                success_codes=success_codes,
                **kwargs
            )

            first_item = next(data, None)

            if not first_item:
                break

            yield first_item

            items_count = 1
            for item in data:
                items_count += 1
                yield item

            if paginated:
                self.next_list_page(params)

            else:
                break
                # limit = offset = items_count
                # method_properties["offset"] = offset
                # method_properties["limit"] = limit

    def add_paginated_list_params(self, params, kwargs):
        limit = kwargs.get('limit')
        offset = kwargs.get('offset')

        paginated = not (None in (limit, offset))

        params["limit"] = limit
        params["offset"] = offset
        return paginated, params

    def next_list_page(self, params):
        params["offset"] += params["limit"]

    def _list_to_string(self, lst: list):
        return ','.join(lst)