from requests import request

from .consts import WHO_ICD_URL
from .requests.client import RequestLoggedAuthClient
from .requests.requester import retry_request


class ICDClient(RequestLoggedAuthClient):

    def __init__(self, api_url=WHO_ICD_URL, http_client=request):
        super().__init__(api_url, http_client)

    def get_headers(self, headers):
        headers = super().get_headers(headers)
        headers['API-Version'] = 'v2'
        return headers