from django.utils.translation import pgettext_lazy

from app.settings.default import env
from django.db.models import TextChoices

WHO_ICD_URL = env.str('WHO_ICD_URL', 'http://127.0.0.1:9000/icd/')


class ParseSessionStatuses(TextChoices):
    NEW = 'new', pgettext_lazy('ParseSessionStatuses', 'New')
    STARTED = 'started', pgettext_lazy('ParseSessionStatuses', 'Started')
    DONE = 'done', pgettext_lazy('ParseSessionStatuses', 'Done')