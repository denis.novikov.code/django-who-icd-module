from typing import Tuple

from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.db.transaction import atomic


class OneUniqueValueMixin:

    ONE_UNIQUE_VALUE_FIELDS: Tuple[Tuple[str, str, str]] = ()
    ONE_UNIQUE_BASE_QS = lambda self: type(self).objects.all()

    def sync_one_unique_values(self):

        pk_field = self._meta.pk.name
        pk = getattr(self, pk_field, None)

        if pk:
            self_filtering = lambda qs: qs.exclude(**{pk_field: pk})

        else:
            self_filtering = lambda qs: qs

        for field_name, unique_value, default_value in self.ONE_UNIQUE_VALUE_FIELDS:

            if getattr(self, field_name) == unique_value:

                qs = self.ONE_UNIQUE_BASE_QS().filter(**{field_name: unique_value})

                qs = self_filtering(qs)

                qs.update(**{field_name: default_value})

    @atomic
    def save(self, *args, **kwargs):
        self.sync_one_unique_values()
        super().save(*args, **kwargs)


class ExcludeOneUniqueValueMixin:

    EXCLUDE_ONE_UNIQUE_VALUE_FIELDS: Tuple[Tuple[str, str]] = ()
    EXCLUDE_ONE_UNIQUE_BASE_QS = lambda self: type(self).objects.all()

    def validate_unique(self, exclude=None):
        super().validate_unique(exclude)

        pk_field = self._meta.pk.name

        try:
            pk = getattr(self, pk_field, None)

        except Exception:
            pk = None

        if pk:
            self_filtering = lambda qs: qs.exclude(**{pk_field: pk})

        else:
            self_filtering = lambda qs: qs

        for field_name, exclude_value in self.EXCLUDE_ONE_UNIQUE_VALUE_FIELDS:

            if (value := getattr(self, field_name)) != exclude_value:

                qs = self.EXCLUDE_ONE_UNIQUE_BASE_QS().filter(**{field_name: value})

                qs = self_filtering(qs)

                if qs.exists():
                    raise ValidationError({'kind': 'Already exist type'})