from django.db import models
from django.utils.translation import pgettext_lazy

from apps.who_icd.contrib.parser.consts import ParseSessionStatuses
from apps.who_icd.contrib.parser.models.mixins.one_unique_value import OneUniqueValueMixin
from model_utils import FieldTracker


class ParseQueueSession(OneUniqueValueMixin, models.Model):

    ONE_UNIQUE_VALUE_FIELDS = ('status', ParseSessionStatuses.STARTED, ParseSessionStatuses.DONE),

    created_at = models.DateTimeField(auto_now=True)

    Statuses = ParseSessionStatuses
    status = models.CharField(
        choices=ParseSessionStatuses.choices,
        default=ParseSessionStatuses.NEW,
        verbose_name=pgettext_lazy('ParseQueueSession', 'Statuses'),
        max_length=7,
    )
    field_tracker = FieldTracker(['status'])

    class Meta:
        verbose_name = pgettext_lazy('icd_parser.ParseQueueSession', 'ParseQueueSession')


class QueueElement(models.Model):

    parse_session = models.ForeignKey(
        ParseQueueSession,
        on_delete=models.CASCADE,
        related_name='elements',
    )

    url = models.CharField(max_length=127)

    language = models.CharField(max_length=2)

    created_at = models.DateTimeField(auto_now=True)

    is_done = models.BooleanField(default=False)

    class Meta:
        verbose_name = pgettext_lazy('icd_parser.QueueElement', 'QueueElement')
        unique_together = ('parse_session', 'language', 'url')


class ParseErrorLog(models.Model):

    url = models.CharField(max_length=127, default='')
    exception = models.CharField(max_length=64, default='')
    message = models.CharField(max_length=512, default='')
    traceback = models.TextField()
    created_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = pgettext_lazy('icd_parser.ParseErrorLog', 'ParserErrorLog')