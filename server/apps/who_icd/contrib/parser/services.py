import time

from django.db import transaction
from django.conf import settings

from .requests.exceptions import get_traceback
from .roots import get_parser_method_name
from .savers import get_data_handler
from .sdk import WhoIcdApiServiceAdapter
from .models import ParseQueueSession, QueueElement, ParseErrorLog
from .utils import get_parser_languages
from ... import consts

sdk = WhoIcdApiServiceAdapter()


def save_error_log(url, e):
    data = get_traceback(e)

    ParseErrorLog.objects.create(
        url=url,
        exception=data['exception'],
        message=data['message'],
        traceback=data['traceback'],
    )


def create_first_queue_element(session):
    releases = '/release/11/mms'
    elements = []

    for lang in get_parser_languages():
        elements.append(QueueElement(parse_session=session, url=releases, language=lang))

    QueueElement.objects.bulk_create(elements)


def check_parsing_session():
    session = ParseQueueSession.objects.filter(is_done=False).first()

    if not session:
        session = ParseQueueSession.objects.create()
        create_first_queue_element(session)


def sdk_parse(url, method_name, language):
    parse = getattr(sdk, method_name)
    return parse(url, language)


def parse_next_element():
    elem = QueueElement.objects.filter(parse_session__status=ParseQueueSession.Statuses.STARTED, is_done=False).first()

    if not elem:
        ParseQueueSession.objects.update(status=ParseQueueSession.Statuses.DONE)
        stop_service()
        return

    try:

        method_name = get_parser_method_name(elem)

        data = sdk_parse(elem.url, method_name, language=elem.language)

        with transaction.atomic():
            handler = get_data_handler(elem)
            handler.save(elem, data)
            handler.create_next_parser_queue_elems(elem, data)

            elem.is_done = True
            elem.save()

    except Exception as e:
        save_error_log(elem.url, e)
        return

    return True


def parse_cycle_start():
    ok = parse_next_element()

    while ok:
        ok = parse_next_element()


def start_parsing():
    check_parsing_session()
    transaction.on_commit(parse_cycle_start)


def icd_api_is_available(except_error=True):
    return sdk.is_running(except_error)


def start_service():
    import os
    if settings.ALLOWED_HOSTS == ['*']:
        #For local working
        # os.system(f"docker container start dj_icd11_whoicd_1")
        ...
    else:
        #For wps
        response_code = os.system(f"docker run --name who_icd -p 9000:80 "
                  f"-e include=2024-01_{consts.ICD_AVAILABLE_LANGUAGES} "
                  "-e acceptLicense=true "
                  "-e saveAnalytics=true "
                  "whoicd/icd-api")

        if response_code == 32000:
            os.system('docker container start who_icd')


def stop_service():
    import os
    if settings.ALLOWED_HOSTS == ['*']:
        # For local working
        #os.system(f"docker container stop dj_icd11_whoicd_1")
        ...
    else:
        #For wps
        os.system(f"docker container stop who_icd")


def prepare_service():

    status = icd_api_is_available()

    tries = 0

    while not status:

        if tries > 5:
            break

        start_service()

        time.sleep(30)
        status, e = icd_api_is_available(except_error=False)

        tries += 1

    else:

        if not status:
            save_error_log(e)

        return status
