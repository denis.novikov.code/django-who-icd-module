import re
from functools import lru_cache

from django.conf import settings
from apps.who_icd.consts import AVAILABLE_LANGUAGES
from apps.who_icd.contrib.parser.consts import WHO_ICD_URL


@lru_cache()
def get_parser_languages():
    return [lang for lang, _ in settings.LANGUAGES if lang in AVAILABLE_LANGUAGES]


def cut_uri(uri: str):
    return uri.replace(WHO_ICD_URL, '/').replace('http://id.who.int/icd/', '/')


def is_entity(uri):
    return bool(re.match(r'/release/\d{2}/\d{4}-\d{2}/\w*/\d*(\?include=[a-zA-Z,]*)?$', uri))


def extend_entity_url(uri):
    return f"{uri}?include=ancestor,descendant"