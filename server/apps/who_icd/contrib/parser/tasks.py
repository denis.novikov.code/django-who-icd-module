from django.db import transaction

from apps.who_icd.contrib.parser.models import ParseQueueSession
from apps.who_icd.contrib.parser.services import create_first_queue_element, parse_next_element, icd_api_is_available, \
    prepare_service

import time
from celery import Task, shared_task
from contextlib import contextmanager
from django.core.cache import cache
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

LOCK_EXPIRE = 60 * 1


@contextmanager
def memcache_lock(lock_id, oid):
    timeout_at = time.monotonic() + LOCK_EXPIRE - 3
    # cache.add fails if the key already exists
    status = cache.add(lock_id, oid, LOCK_EXPIRE)
    try:
        yield status

    finally:
        # memcache delete is very slow, but we have to use it to take
        # advantage of using add() for atomic locking
        if time.monotonic() < timeout_at and status:
            # don't release the lock if we exceeded the timeout
            # to lessen the chance of releasing an expired lock
            # owned by someone else
            # also don't release the lock if we didn't acquire it
            cache.delete(lock_id)


class SingletonTask(Task):

    def __call__(self, *args, **kwargs):
        lock_id = '{0}-lock'.format(self.name)

        logger.debug('Start task: %s', self.name)
        with memcache_lock(lock_id, self.app.oid) as acquired:
            if acquired:
                super().__call__(*args, **kwargs)

        logger.debug('Task %s already running' % self.name)
        print('Task %s already running' % self.name)


@shared_task(base=SingletonTask)
def parse_next_elem_task():
    ok = parse_next_element()

    if ok:
        parse_next_elem_task.delay()


@shared_task(base=SingletonTask)
def start_parsing_task(session_id):
    session = ParseQueueSession.objects.get(pk=session_id)
    create_first_queue_element(session)

    session.status = session.Statuses.STARTED
    session.save(update_fields=['status'])

    transaction.on_commit(lambda: continue_parsing_task.delay())


@shared_task(base=SingletonTask)
def continue_parsing_task():
    service_is_ok = prepare_service()

    if service_is_ok:
        parse_next_elem_task.delay()