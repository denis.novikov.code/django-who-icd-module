from django.apps import AppConfig
from django.utils.translation import pgettext_lazy


class ParserConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    label = 'icd_parser'
    name = 'apps.who_icd.contrib.parser'
    verbose_name = pgettext_lazy('ParserConfig', 'WHO ICD: Parser')

    def ready(self):
        from . import dispatchers