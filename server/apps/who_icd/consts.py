from django.db import models
from django.utils.translation import pgettext_lazy

from app.settings.default import env

ICD_AVAILABLE_LANGUAGES = env.str('ICD_AVAILABLE_LANGUAGES', 'ar-cs-en-es-fr-pt-ru-tr-uz-zh')
AVAILABLE_LANGUAGES = ICD_AVAILABLE_LANGUAGES.split('-')


class ClassKinds(models.TextChoices):
    BLOCK = 'block', pgettext_lazy('ClassKinds', 'Block')
    CATEGORY = 'category', pgettext_lazy('ClassKinds', 'Category')
    CHAPTER = 'chapter', pgettext_lazy('ClassKinds', 'Chapter')
    WINDOW = 'window', pgettext_lazy('ClassKinds', 'Window')


class Countries(models.TextChoices):

    UKRAINE = 'ukraine', pgettext_lazy('Countries', 'Ukraine')
    INDIA = 'india', pgettext_lazy('Countries', 'India')
    MONGOLIA = 'mongolia', pgettext_lazy('Countries', 'Mongolia')
    GERMANY = 'germany', pgettext_lazy('Countries', 'Germany')