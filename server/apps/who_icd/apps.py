from django.apps import AppConfig
from django.utils.translation import pgettext_lazy


class WhoIcdConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    label = 'who_icd'
    name = 'apps.who_icd'
    verbose_name = pgettext_lazy('ParserConfig', 'WHO ICD')
