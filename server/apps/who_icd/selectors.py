from functools import lru_cache
from .models import Entity


@lru_cache(maxsize=64)
def get_entity(icd_id):
    return Entity.objects.get(icd_id=icd_id)