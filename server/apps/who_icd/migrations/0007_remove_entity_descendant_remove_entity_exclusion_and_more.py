# Generated by Django 5.0.4 on 2024-04-30 15:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('who_icd', '0006_alter_entity_title_alter_entity_title_en_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='entity',
            name='descendant',
        ),
        migrations.RemoveField(
            model_name='entity',
            name='exclusion',
        ),
        migrations.RemoveField(
            model_name='entity',
            name='foundation_child_elsewhere',
        ),
        migrations.AddField(
            model_name='entity',
            name='specified_name',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Specified name'),
        ),
    ]
