# Generated by Django 5.0.4 on 2024-05-01 09:45

import apps.who_icd.models.fields.multiple_choice
import ckeditor.fields
import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('who_icd', '0011_remove_entity_id_parent'),
    ]

    operations = [
        migrations.CreateModel(
            name='SymptomTag',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField(blank=True, max_length=255, null=True, unique=True, verbose_name='Slug')),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='Name')),
                ('name_ru', models.CharField(max_length=64, null=True, unique=True, verbose_name='Name')),
                ('name_en', models.CharField(max_length=64, null=True, unique=True, verbose_name='Name')),
                ('name_fr', models.CharField(max_length=64, null=True, unique=True, verbose_name='Name')),
            ],
            options={
                'verbose_name': 'SymptomTag',
                'verbose_name_plural': 'SymptomTag',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Details',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('symptoms_intro', ckeditor.fields.RichTextField(blank=True, default='', max_length=1024, verbose_name='Symptoms intro')),
                ('symptoms_intro_ru', ckeditor.fields.RichTextField(blank=True, default='', max_length=1024, null=True, verbose_name='Symptoms intro')),
                ('symptoms_intro_en', ckeditor.fields.RichTextField(blank=True, default='', max_length=1024, null=True, verbose_name='Symptoms intro')),
                ('symptoms_intro_fr', ckeditor.fields.RichTextField(blank=True, default='', max_length=1024, null=True, verbose_name='Symptoms intro')),
                ('diagnostics', ckeditor.fields.RichTextField(blank=True, default='', verbose_name='Diagnostics')),
                ('diagnostics_ru', ckeditor.fields.RichTextField(blank=True, default='', null=True, verbose_name='Diagnostics')),
                ('diagnostics_en', ckeditor.fields.RichTextField(blank=True, default='', null=True, verbose_name='Diagnostics')),
                ('diagnostics_fr', ckeditor.fields.RichTextField(blank=True, default='', null=True, verbose_name='Diagnostics')),
                ('treatment_intro', ckeditor.fields.RichTextField(blank=True, default='', verbose_name='Treatment intro')),
                ('treatment_intro_ru', ckeditor.fields.RichTextField(blank=True, default='', null=True, verbose_name='Treatment intro')),
                ('treatment_intro_en', ckeditor.fields.RichTextField(blank=True, default='', null=True, verbose_name='Treatment intro')),
                ('treatment_intro_fr', ckeditor.fields.RichTextField(blank=True, default='', null=True, verbose_name='Treatment intro')),
                ('prevention', ckeditor.fields.RichTextField(blank=True, default='', verbose_name='Prevention')),
                ('prevention_ru', ckeditor.fields.RichTextField(blank=True, default='', null=True, verbose_name='Prevention')),
                ('prevention_en', ckeditor.fields.RichTextField(blank=True, default='', null=True, verbose_name='Prevention')),
                ('prevention_fr', ckeditor.fields.RichTextField(blank=True, default='', null=True, verbose_name='Prevention')),
                ('entity', models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='details', to='who_icd.entity', verbose_name='Details')),
            ],
        ),
        migrations.CreateModel(
            name='Symptom',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', ckeditor.fields.RichTextField(blank=True, default='', max_length=255, verbose_name='Description')),
                ('description_ru', ckeditor.fields.RichTextField(blank=True, default='', max_length=255, null=True, verbose_name='Description')),
                ('description_en', ckeditor.fields.RichTextField(blank=True, default='', max_length=255, null=True, verbose_name='Description')),
                ('description_fr', ckeditor.fields.RichTextField(blank=True, default='', max_length=255, null=True, verbose_name='Description')),
                ('order', models.PositiveIntegerField(default=0)),
                ('details', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='symptoms', to='who_icd.details')),
                ('tags', models.ManyToManyField(blank=True, related_name='composite_symptom', to='who_icd.symptomtag', verbose_name='Symptoms tags')),
            ],
            options={
                'verbose_name': 'Symptom',
                'verbose_name_plural': 'Symptom',
                'ordering': ('order',),
            },
        ),
        migrations.CreateModel(
            name='TreatmentMethod',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', ckeditor.fields.RichTextField(default='', max_length=1024, verbose_name='Description')),
                ('description_ru', ckeditor.fields.RichTextField(default='', max_length=1024, null=True, verbose_name='Description')),
                ('description_en', ckeditor.fields.RichTextField(default='', max_length=1024, null=True, verbose_name='Description')),
                ('description_fr', ckeditor.fields.RichTextField(default='', max_length=1024, null=True, verbose_name='Description')),
                ('traditional', apps.who_icd.models.fields.multiple_choice.MultipleChoiceArrayField(base_field=models.CharField(choices=[('ukraine', 'Ukraine'), ('india', 'India'), ('mongolia', 'Mongolia'), ('germany', 'Germany')], max_length=8), blank=True, default=list, size=None, verbose_name='Сountries where it is a traditional method of treatment')),
                ('non_traditional', apps.who_icd.models.fields.multiple_choice.MultipleChoiceArrayField(base_field=models.CharField(choices=[('ukraine', 'Ukraine'), ('india', 'India'), ('mongolia', 'Mongolia'), ('germany', 'Germany')], max_length=8), blank=True, default=list, size=None, verbose_name='Сountries where it is a non-traditional method of treatment')),
                ('order', models.PositiveIntegerField(default=0)),
                ('details', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='treatment_methods', to='who_icd.details')),
            ],
            options={
                'verbose_name': 'TreatmentMethod',
                'verbose_name_plural': 'TreatmentMethods',
                'ordering': ('order',),
            },
        ),
    ]
