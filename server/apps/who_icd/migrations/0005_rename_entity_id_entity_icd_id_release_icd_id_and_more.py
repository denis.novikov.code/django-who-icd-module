# Generated by Django 5.0.4 on 2024-04-30 11:04

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('who_icd', '0004_entity_definition_en_entity_definition_fr_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='entity',
            old_name='entity_id',
            new_name='icd_id',
        ),
        migrations.AddField(
            model_name='release',
            name='icd_id',
            field=models.CharField(default=None, max_length=127, unique=True, verbose_name='Icd id'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='entity',
            name='release',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='who_icd.release', to_field='icd_id'),
        ),
    ]
