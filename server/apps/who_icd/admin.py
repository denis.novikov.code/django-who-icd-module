from django.contrib import admin
from django.contrib.admin import site as admin_site
from django.db.models import Count
from django.forms import ModelForm
from django.utils.safestring import mark_safe
from django.contrib.admin.widgets import ForeignKeyRawIdWidget

import nested_admin
from modeltranslation.admin import TabbedDjangoJqueryTranslationAdmin, TranslationStackedInline, TranslationTabularInline

from .models import Entity, Release, Details, Symptom, TreatmentMethod, SymptomTag
from .utils.admin import admin_changelist_url


@admin.register(Release)
class ReleaseModelAdmin(TabbedDjangoJqueryTranslationAdmin):
    list_display = ('__str__', 'date', 'available_languages')


class EntityModelForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['parent'].widget = ForeignKeyRawIdWidget(self.instance._meta.get_field('parent').remote_field, admin_site)


class SymptomNestedInline(nested_admin.NestedTabularInlineMixin, TranslationTabularInline):
    model = Symptom
    extra = 0
    can_delete = True
    sortable_field_name = 'order'


class TreatmentMethodNestedInline(nested_admin.NestedStackedInlineMixin, TranslationStackedInline):
    model = TreatmentMethod
    max_num = 1
    extra = 0


class DetailsNestedInline(nested_admin.NestedStackedInlineMixin, TranslationStackedInline):
    model = Details
    max_num = 1
    extra = 0
    can_delete = False
    inlines = [SymptomNestedInline, TreatmentMethodNestedInline]


@admin.register(Entity)
class EntityModelAdmin(nested_admin.NestedModelAdminMixin, TabbedDjangoJqueryTranslationAdmin):
    list_display = ['get_parent', 'icd_id', 'id', 'code', 'title', 'class_kind',  'get_children']
    search_fields = ('icd_id', 'parent__icd_id', 'id', 'code',)
    list_filter = ('class_kind', 'depth')
    list_display_links = 'icd_id',
    form = EntityModelForm
    inlines = [DetailsNestedInline]
    readonly_fields = ('metadata_en', )
    exclude = ('metadata',)

    def get_children(self, obj):

        if obj.child_cnt:
            base_url = admin_changelist_url(Entity)

            url = f"{base_url}?depth={obj.depth+1}&q={obj.icd_id}"

            return mark_safe(f'<a href="{url}">Children ({obj.child_cnt}) &#8680</a>')

        return ''

    def get_parent(self, obj):
        base_url = admin_changelist_url(Entity)

        if obj.parent_id:
            url = f"{base_url}?depth={obj.depth-1}"

            if obj.parent.parent_id:
                url = f"{url}&q={obj.parent.parent_id}"

            return mark_safe(f'<a href="{url}">&cularr; Parent</a>')

        return ''

    def get_queryset(self, request):

        queryset = super().get_queryset(request)

        return queryset.select_related('parent').annotate(child_cnt=Count('children'))


@admin.register(SymptomTag)
class SymptomTagModelAdmin(TabbedDjangoJqueryTranslationAdmin):
    list_display = ('name', 'slug')


@admin.register(Symptom)
class SymptomModelAdmin(TabbedDjangoJqueryTranslationAdmin):
    list_display = ('id', 'details', 'description')


@admin.register(TreatmentMethod)
class TreatmentMethodModelAdmin(TabbedDjangoJqueryTranslationAdmin):
    list_display = ('id', 'details', 'description', 'traditional', 'non_traditional')