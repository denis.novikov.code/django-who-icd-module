from modeltranslation.decorators import register
from modeltranslation.translator import TranslationOptions

from apps.who_icd.models import (
    Release, Entity,
    Details, SymptomTag,
    Symptom, TreatmentMethod
)

__all__ = 'ReleaseTranslationOptions', 'EntityTranslationOptions'


@register(Release)
class ReleaseTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(Entity)
class EntityTranslationOptions(TranslationOptions):
    fields = ('title', 'definition', 'specified_name', 'metadata')


@register(Details)
class DetailsTranslationOptions(TranslationOptions):
    fields = ('symptoms_intro', 'diagnostics', 'treatment_intro', 'prevention')


@register(SymptomTag)
class SymptomTagTranslationOptions(TranslationOptions):
    fields = ('name', )


@register(Symptom)
class SymptomTranslationOptions(TranslationOptions):
    fields = ('description', )


@register(TreatmentMethod)
class TreatmentMethodTranslationOptions(TranslationOptions):
    fields = ('description', )