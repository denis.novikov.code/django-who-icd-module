from django.db import models
from django.utils.translation import pgettext_lazy

from apps.who_icd.consts import ClassKinds
from apps.who_icd.models.mixins.dates import TimestampsMixin
from apps.who_icd.models.mixins.meta import ModelWithMetadata

__all__ = ('Entity',)


class Entity(ModelWithMetadata, TimestampsMixin):

    title = models.CharField(pgettext_lazy('who_icd.Release', 'Title'), max_length=1536)

    specified_name = models.CharField(
        pgettext_lazy('who_icd.Release', 'Specified name'),
        max_length=255, blank=True, default=''
    )

    definition = models.TextField(
        pgettext_lazy('who_icd.Release', 'Definition'),
        max_length=1024, blank=True, default=''
    )

    icd_id = models.CharField(
        pgettext_lazy('Entity', 'Icd id'),
        max_length=127,
        unique=True
    )

    # release = models.ForeignKey(
    #     'who_icd.Release',
    #     on_delete=models.CASCADE,
    #     to_field='icd_id',
    #     related_name='entities',
    #     null=True
    # )

    parent = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        to_field='icd_id',
        related_name='children',
        null=True,
        blank=True
    )

    # id_parent = models.CharField(pgettext_lazy('Entity', 'tmp parent id'), max_length=127, blank=True, default='')

    code = models.CharField(
        pgettext_lazy('Entity', 'Code'),
        max_length=10,
        default='',
        db_index=True
    )

    KINDS = ClassKinds
    class_kind = models.CharField(
        pgettext_lazy('Entity', 'Class kind'),
        max_length=8,
        choices=ClassKinds.choices,
        db_index=True
    )

    depth = models.PositiveSmallIntegerField(
        pgettext_lazy('Entity', 'Depth')
    )

    # foundation_child_elsewhere = models.ManyToManyField(
    #     'self',
    #     verbose_name=pgettext_lazy('Entity', 'Foundation child elsewhere'),
    #     blank=True
    # )
    #
    # descendant = models.ManyToManyField(
    #     'self',
    #     verbose_name=pgettext_lazy('Entity', 'Descendant'),
    #     blank=True
    # )
    #
    # exclusion = models.ManyToManyField(
    #     'self',
    #     verbose_name=pgettext_lazy('Entity', 'exclusion'),
    #     blank=True
    # )

    class Meta:
        verbose_name = pgettext_lazy('who_icd.Entity', 'Entity')
        verbose_name_plural = pgettext_lazy('who_icd.Entity', 'Entities')

    def __str__(self):
        return f'{self.title} {self.code}'

