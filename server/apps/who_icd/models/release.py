from django.db import models
from django.utils.translation import pgettext_lazy

from .fields.multiple_choice import MultipleChoiceArrayField
from .mixins.dates import TimestampsMixin
from .mixins.meta import ModelWithMetadata
from ..consts import AVAILABLE_LANGUAGES

__all__ = ('Release',)


class Release(ModelWithMetadata, TimestampsMixin):

    icd_id = models.CharField(
        pgettext_lazy('Entity', 'Icd id'),
        max_length=127,
        unique=True
    )
    release_id = models.CharField(pgettext_lazy('Entity', 'Icd id'), max_length=127, unique=True)
    title = models.CharField(pgettext_lazy('who_icd.Release', 'Title'), max_length=255)
    date = models.DateField(verbose_name=pgettext_lazy('who_icd.Release', 'Date'))
    available_languages = MultipleChoiceArrayField(
        models.CharField(
            max_length=2,
            choices=zip(AVAILABLE_LANGUAGES, AVAILABLE_LANGUAGES),
            blank=True
        ),
        blank=True, default=list,
        verbose_name=pgettext_lazy('who_icd.Release', 'Available languages'),
    )

    class Meta:
        verbose_name = pgettext_lazy('who_icd.Release', 'Release')
        verbose_name_plural = pgettext_lazy('who_icd.Release', 'Releases')

    def __str__(self):
        return self.title

