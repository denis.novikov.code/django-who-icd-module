from django.db import models
from django.utils.translation import pgettext_lazy
from ckeditor.fields import RichTextField

from apps.who_icd.consts import Countries
from .fields.multiple_choice import MultipleChoiceArrayField
from .mixins.slug import SlugMixin


class Details(models.Model):

    entity = models.OneToOneField(
        'who_icd.Entity',
        on_delete=models.SET_NULL,
        null=True,
        related_name='details',
        verbose_name=pgettext_lazy('who_icd.Entity', 'Details')
    )

    symptoms_intro = RichTextField(
        pgettext_lazy('who_icd.Entity', 'Symptoms intro'),
        blank=True, default='', max_length=1024
    )

    diagnostics = RichTextField(
        pgettext_lazy('who_icd.Entity', 'Diagnostics'),
        blank=True, default=''
    )

    treatment_intro = RichTextField(
        pgettext_lazy('who_icd.Entity', 'Treatment intro'),
        blank=True, default='',
    )

    prevention = RichTextField(
        pgettext_lazy('who_icd.Entity', 'Prevention'),
        blank=True, default='',
    )


class SymptomTag(SlugMixin):

    SLUGIFY_FIELD = 'name'

    name = models.CharField(
        pgettext_lazy('who_icd.Entity', 'Name'),
        max_length=64, unique=True
    )

    class Meta:
        ordering = ('name',)
        verbose_name = pgettext_lazy('who_icd.Symptom', 'SymptomTag')
        verbose_name_plural = pgettext_lazy('who_icd.Symptom', 'SymptomTag')

    def __str__(self):
        return self.name


class Symptom(models.Model):

    details = models.ForeignKey(
        Details, on_delete=models.CASCADE,
        related_name='symptoms',
    )

    description = RichTextField(
        pgettext_lazy('who_icd.Symptom', 'Description'),
        blank=True, default='', max_length=255
    )

    tags = models.ManyToManyField(
        SymptomTag, blank=True,
        verbose_name=pgettext_lazy('who_icd.Symptom', 'Symptoms tags'),
        related_name='composite_symptom'
    )

    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        ordering = ('order',)
        verbose_name = pgettext_lazy('who_icd.Symptom', 'Symptom')
        verbose_name_plural = pgettext_lazy('who_icd.Symptom', 'Symptom')

    def __str__(self):
        return f'{self.pk} {self.description}'


class TreatmentMethod(models.Model):

    details = models.ForeignKey(
        Details, on_delete=models.CASCADE,
        related_name='treatment_methods',
    )

    description = RichTextField(
        pgettext_lazy('who_icd.TreatmentMethod', 'Description'),
        default='', max_length=1024
    )

    traditional = MultipleChoiceArrayField(
        base_field=models.CharField(choices=Countries.choices, max_length=8), blank=True, default=list,
        verbose_name=pgettext_lazy(
            'who_icd.TreatmentMethod',
            'Сountries where it is a traditional method of treatment'
        ),
    )

    non_traditional = MultipleChoiceArrayField(
        base_field=models.CharField(choices=Countries.choices, max_length=8), blank=True, default=list,
        verbose_name=pgettext_lazy(
            'who_icd.TreatmentMethod',
            'Сountries where it is a non-traditional method of treatment'
        ),
    )

    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        ordering = ('order',)
        verbose_name = pgettext_lazy('who_icd.TreatmentMethod', 'TreatmentMethod')
        verbose_name_plural = pgettext_lazy('who_icd.TreatmentMethod', 'TreatmentMethods')

