from django import forms
from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.utils import prefix_validation_error
from django.core import exceptions
from django.db import models
from django.forms import SelectMultiple, TypedMultipleChoiceField

__all__ = ('MultipleChoiceArrayField', 'MultipleChoiceField')


class ArraySelectMultiple(SelectMultiple):
    def value_omitted_from_data(self, data, files, name):
        return False


class MultipleChoiceArrayField(ArrayField):

    def formfield(self, **kwargs):
        defaults = {
            'form_class': forms.TypedMultipleChoiceField,
            'choices': self.base_field.choices,
            'coerce': self.base_field.to_python,
            'widget': ArraySelectMultiple
        }
        defaults.update(kwargs)

        return super(ArrayField, self).formfield(**defaults)

    def validate(self, value, model_instance):
        for index, part in enumerate(value):

            super(ArrayField, self).validate(part, model_instance)
            try:
                self.base_field.validate(part, model_instance)
            except exceptions.ValidationError as error:
                raise prefix_validation_error(
                    error,
                    prefix=self.error_messages['item_invalid'],
                    code='item_invalid',
                    params={'nth': index + 1},
                )
        if isinstance(self.base_field, ArrayField):
            if len({len(i) for i in value}) > 1:
                raise exceptions.ValidationError(
                    self.error_messages['nested_array_mismatch'],
                    code='nested_array_mismatch',
                )


class MultipleChoiceField(ArrayField):

    def __init__(self, choices, max_length, **kwargs):
        kwargs['base_field'] = models.CharField(max_length=max_length, choices=choices)
        super().__init__(choices=choices, max_length=max_length, **kwargs)

    def formfield(self, **kwargs):
        formfield = TypedMultipleChoiceField(choices=self.choices)
        return formfield

    def validate(self, value, model_instance):
        for index, part in enumerate(value):

            super(ArrayField, self).validate(part, model_instance)
            try:
                self.base_field.validate(part, model_instance)
            except exceptions.ValidationError as error:
                raise prefix_validation_error(
                    error,
                    prefix=self.error_messages['item_invalid'],
                    code='item_invalid',
                    params={'nth': index + 1},
                )
        if isinstance(self.base_field, ArrayField):
            if len({len(i) for i in value}) > 1:
                raise exceptions.ValidationError(
                    self.error_messages['nested_array_mismatch'],
                    code='nested_array_mismatch',
                )