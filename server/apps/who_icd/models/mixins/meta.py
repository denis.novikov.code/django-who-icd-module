from typing import Any, Dict

from django.core.serializers.json import DjangoJSONEncoder
from django.db import models

__all__ = (
    'ModelWithMetadata',
    'ModelWithPrivateMetadata',
)


class ModelWithMetadata(models.Model):
    metadata = models.JSONField(
        blank=True,
        null=True,
        default=dict,
        encoder=DjangoJSONEncoder
    )

    class Meta:
        abstract = True

    def get_meta(self, namespace: str, client: str = None) -> Dict:
        meta = self.metadata.get(namespace, {}).get(client, {})
        if client:
            meta = meta.get(client, {})

        return meta

    def store_meta(self, namespace: str, item: Any, client: str = None):
        if namespace not in self.metadata:
            self.metadata[namespace] = {}

        if client:
            self.metadata[namespace][str(client)] = item

        else:
            self.metadata[namespace] = item

    def clear_stored_meta_for_client(self, namespace: str, client: str = None):
        if client:
            self.metadata.get(namespace, {}).pop(client, None)
        else:
            self.metadata.pop(namespace, None)


class ModelWithPrivateMetadata(ModelWithMetadata):
    private_metadata = models.JSONField(
        blank=True,
        null=True,
        default=dict,
        encoder=DjangoJSONEncoder
    )

    class Meta:
        abstract = True

    def get_private_meta(self, namespace: str, client: str) -> Dict:
        return self.private_metadata.get(namespace, {}).get(client, {})

    def store_private_meta(self, namespace: str, client: str, item: Any):
        if namespace not in self.private_metadata:
            self.private_metadata[namespace] = {}
        self.private_metadata[namespace][str(client)] = item

    def clear_stored_private_meta_for_client(self, namespace: str, client: str):
        self.private_metadata.get(namespace, {}).pop(client, None)
