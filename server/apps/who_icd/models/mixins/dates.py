from django.contrib.postgres.indexes import BrinIndex
from django.db import models
from django.utils.translation import pgettext_lazy

__all__ = (
    'CreatedAtMixin',
    'DateTimeMixin',
    'TimestampsMixin',
)


class CreatedAtMixin(models.Model):
    created_at = models.DateTimeField(
        pgettext_lazy('created_at', 'Created at'),
        auto_now_add=True
    )

    class Meta:
        abstract = True


class DateTimeMixin(CreatedAtMixin):
    updated_at = models.DateTimeField(
        pgettext_lazy('datetime_mixin', 'Updated at'),
        auto_now=True
    )

    class Meta:
        abstract = True


class TimestampsMixin(models.Model):
    """
    Timestamps abstract model

    Attrs:
        created_at (DateTimeField): created_at timestamp
        updated_at (DateTimeField): updated_at timestamp
    """
    created_at = models.DateTimeField(
        verbose_name=pgettext_lazy('shared.TimestampsMixin', 'Created at'),
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=pgettext_lazy('shared.TimestampsMixin', 'Updated at'),
        auto_now=True
    )

    class Meta:
        abstract = True
        ordering = ['-created_at']
        indexes = (
            BrinIndex(fields=['created_at']),
        )
