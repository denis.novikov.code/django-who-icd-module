from unidecode import unidecode

from django.db import models
from django.utils.crypto import get_random_string
from django.utils.text import slugify
from django.utils.translation import pgettext_lazy

__all__ = (
    'SlugMixin',
)


class SlugMixin(models.Model):
    SLUGIFY_FIELD: str
    
    slug = models.SlugField(
        pgettext_lazy('slug_mixin', 'Slug'),
        max_length=255,
        unique=True,
        blank=True,
        null=True
    )

    class Meta:
        abstract = True
        
    @property
    def to_slugify(self):
        return getattr(self, self.SLUGIFY_FIELD)
    
    def is_unique_slug(self, slug):
        qs = self.__class__.objects.filter(slug=slug)
        if self.id:
            qs = qs.exclude(id=self.id)
        return not qs.exists()

    def generate_slug(self):
        slug = slugify(unidecode(self.to_slugify))
        while True:
            if self.is_unique_slug(slug):
                break
            slug = f"{slug}-{get_random_string(2)}"

        return slug
    
    def save(self, *args, **kwargs):
        if not self.slug or not self.is_unique_slug(self.slug):
            self.slug = self.generate_slug()
        return super().save(*args, **kwargs)