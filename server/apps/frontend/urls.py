from django.urls import include, re_path, path
from django.views.generic import TemplateView

app_name = 'frontend'

entry = (
    re_path(
        r'(?P<path>[\w/._-]{0,256})',
        TemplateView.as_view(template_name='frontend/index.jinja'),
        name='application'
    ),
)

urlpatterns = (
    path('', include((entry, app_name), 'entry')),
)
