from django.conf import settings
from django.urls import path, include, re_path
from django.contrib import admin
from django.contrib.staticfiles.urls import static


urlpatterns = [
    path('admin/', admin.site.urls),
    # url(r'', include('apps.frontend.urls')),
]

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [path(r'rosetta/', include('rosetta.urls'))]

if settings.DEBUG:
    urlpatterns += (
        static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) +
        static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    )

    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns += [
            path('__debug__/', include(debug_toolbar.urls)),
        ]
