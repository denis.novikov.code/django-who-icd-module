upstream {{ PROJECT_NAME }}_server {
    server unix:{{DJANGO_SOCKETS_PATH}}/{{ PROJECT_NAME }}.socket fail_timeout=0;
}

upstream {{ PROJECT_NAME }}_asgi_server {
    server unix:ASGI_SOCKET fail_timeout=0;
}

{% if HAS_CENTRIFUGO %}
upstream centrifugo {
    ip_hash;
    server {{ CENTRIFUGO_HOST }}:{{ CENTRIFUGO_PORT }};
}

map $http_upgrade $connection_upgrade {
    default upgrade;
    ''      close;
}

{% endif %}

{% if HAS_IMGPROXY %}
upstream imgproxy {
    ip_hash;
    server localhost:8080;
}

server {
    server_name imgproxy;
    listen bestev.webcase-dev.com:48210;
    location / {
        proxy_pass http://imgproxy;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}
{% endif %}

# FOR SSL Redirect
#server {
#    server_name bestev.webcase-dev.com www.bestev.webcase-dev.com;
#    listen 80;
#    return 302 https://bestev.webcase-dev.com$request_uri;
#}

server {
    listen {{ PORT }} default_server;
    listen [::]:{{ PORT }} default_server ipv6only=on;
    server_name _;
    
    # FOR SSL
    #server_name bestev.webcase-dev.com;
    #listen bestev.webcase-dev.com:443 ssl;
    #access_log off;

    #ssl_certificate /etc/letsencrypt/live/bestev.webcase-dev.com/fullchain.pem;
    #ssl_certificate_key /etc/letsencrypt/live/bestev.webcase-dev.com/privkey.pem;
    #ssl_trusted_certificate /etc/letsencrypt/live/bestev.webcase-dev.com/chain.pem;

    #ssl_stapling on;
    #ssl_stapling_verify on;

    #add_header Strict-Transport-Security "max-age=31536000";
    error_log /var/log/nginx/error.log crit;

    open_file_cache max=200000 inactive=20s;
    open_file_cache_valid 30s;
    open_file_cache_min_uses 2;
    open_file_cache_errors on;

    access_log off;
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;

    gzip on;
    gzip_comp_level    5;
    gzip_min_length 256;
    gzip_proxied any;
    gzip_types
      application/atom+xml
       application/javascript
       application/json
       application/ld+json
       application/manifest+json
       application/rss+xml
       application/vnd.geo+json
       application/vnd.ms-fontobject
       application/x-font-ttf
       application/x-web-app-manifest+json
       application/xhtml+xml
       application/xml
       font/opentype
       image/bmp
       image/svg+xml
       image/x-icon
       text/cache-manifest
       text/css
       text/plain
       text/vcard
       text/vnd.rim.location.xloc
       text/vtt
       text/x-component
       text/x-cross-domain-policy;

    gzip_disable msie6;

    keepalive_timeout 30;
    keepalive_requests 100000;
    reset_timedout_connection on;
    client_body_timeout 10;
    send_timeout 2;

    client_max_body_size 4G;

    # Your Django project's media files - amend as required
    location /uploads  {
        alias {{ PATH_TO_UPLOADS }};
        expires 30d;
    }

    # your Django project's static files - amend as required
    location /static {
        alias {{ PATH_TO_STATIC }};
        expires 30d;
    }

    {% if HAS_ASGI %}
    # asgi
    location ASGI_BASE_URL {
        # proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://{{ PROJECT_NAME }}_asgi_server;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }
    {% endif %}

    {% if HAS_CENTRIFUGO %}
    # Centrifugo start 
    location /centrifugo/ {
       rewrite ^/centrifugo/(.*)        /$1 break;
       proxy_pass_header Server;
       proxy_set_header Host $http_host;
       proxy_redirect off;
       proxy_set_header X-Real-IP $remote_addr;
       proxy_set_header X-Scheme $scheme;
       proxy_pass http://centrifugo;
       proxy_http_version 1.1;
       proxy_set_header Upgrade $http_upgrade;
       proxy_set_header Connection $connection_upgrade;
    }
    location /centrifugo/connection {
       rewrite ^/centrifugo(.*)        $1 break;

       proxy_next_upstream error;
       gzip on;
       gzip_min_length 1000;
       gzip_proxied any;
       proxy_buffering off;
       keepalive_timeout 65;
       proxy_pass http://centrifugo;
       proxy_read_timeout 60s;
       proxy_set_header X-Real-IP $remote_addr;
       proxy_set_header X-Scheme $scheme;
       proxy_set_header Host $http_host;
       proxy_http_version 1.1;
       proxy_set_header Upgrade $http_upgrade;
       proxy_set_header Connection $connection_upgrade;
    }
    # Centrifugo end
    {% endif %}

    # gunicorn
    location / {
        # proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_redirect off;
        proxy_pass http://{{ PROJECT_NAME }}_server;
    }

    # uwsgi
    # location / {
    #     uwsgi_pass 0.0.0.0:9000;
    #     include uwsgi_params;
    # }
    
    # FOR SSL
    #location /.well-known {
    #    root /var/www/html;
    #}
}

{% if HAS_IMGPROXY %}


upstream imgproxy_server {
    ip_hash;
    server localhost:48210;
}


#server {
#    server_name img.savun.webcase-dev.com www.img.savun.webcase-dev.com;
#    listen 443 ssl http2;
#
#    ssl_certificate /etc/letsencrypt/live/img.savun.webcase-dev.com/fullchain.pem;
#    ssl_certificate_key /etc/letsencrypt/live/img.savun.webcase-dev.com/privkey.pem;
#    ssl_trusted_certificate /etc/letsencrypt/live/img.savun.webcase-dev.com/chain.pem;
#
#    ssl_stapling on;
#    ssl_stapling_verify on;
#
#    # this for certbot to renew certificates when ssl is already enabled
#    location /.well-known {
#        root /var/www/html;
#    }
#
#    # redirect everything else to https
#    location / {
#        return 302 https://$host$request_uri;
#   }
#}

server {
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;
    server_name _;

    # server_name img.savun.webcase-dev.com;
    # listen img.savun.webcase-dev.com:443 ssl;
    #
    # listen 443 ssl http2 default_server;
    # listen [::]:443 ssl http2 default_server ipv6only=on;
    #
    # access_log off;
    #
    # ssl_certificate /etc/letsencrypt/live/img.savun.webcase-dev.com/fullchain.pem;
    # ssl_certificate_key /etc/letsencrypt/live/img.savun.webcase-dev.com/privkey.pem;
    # ssl_trusted_certificate /etc/letsencrypt/live/img.savun.webcase-dev.com/chain.pem;
    #
    # ssl_stapling on;
    # ssl_stapling_verify on;

    add_header Strict-Transport-Security "max-age=31536000";

    error_log /var/log/nginx/error.log crit;

    open_file_cache max=200000 inactive=20s;
    open_file_cache_valid 30s;
    open_file_cache_min_uses 2;
    open_file_cache_errors on;

    access_log off;
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;

    gzip on;
    gzip_comp_level    5;
    gzip_min_length 256;
    gzip_proxied any;
    gzip_types
      application/atom+xml
       application/javascript
       application/json
       application/ld+json
       application/manifest+json
       application/rss+xml
       application/vnd.geo+json
       application/vnd.ms-fontobject
       application/x-font-ttf
       application/x-web-app-manifest+json
       application/xhtml+xml
       application/xml
       font/opentype
       image/bmp
       image/svg+xml
       image/x-icon
       text/cache-manifest
       text/css
       text/plain
       text/vcard
       text/vnd.rim.location.xloc
       text/vtt
       text/x-component
       text/x-cross-domain-policy;

    gzip_disable msie6;

    keepalive_timeout 30;
    keepalive_requests 100000;
    reset_timedout_connection on;
    client_body_timeout 10;
    send_timeout 2;

    client_max_body_size 4G;

    error_page 502 /502.html;

    location = /502.html {
        root /home/savun_inc/savun_inc/savun_inc/server/markup/templates/;
        internal;
    }

    location / {
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header Host $http_host;

      proxy_redirect off;
      proxy_pass http://imgproxy_server;
    }

    location /.well-known {
        root /var/www/html;
    }

}
{% endif %}