import sys, pathlib, os
from fabric import api, contrib

# Add project directories
sys.path.append(str(pathlib.Path(__file__).parent.parent.parent))
sys.path.append(str(pathlib.Path(__file__).parent.parent))
sys.path.append(str(pathlib.Path(__file__).parent))

from deploy import server, services

# SSH CREDENTIALS

# Host to connect
api.env.hosts = [os.environ['DEPLOY_IP']]
api.env.user = os.environ.get("DEPLOY_USER", "root")
# api.env.port = 22
# api.env.password = ''

# !!! IMPORTANT
# Absolute path to ssh private key. Add it to your invronment
api.env.key_filename = os.environ['DEPLOY_KEY']


# Rewrite it
@api.task
def pull():
    """
    Pulls last updates from project's repository.
    """
    server.project.pull()


@api.task
def migrate():
    """
    Makes migrations.
    """
    server.project.migrate()


@api.task
def collectstatic():
    """
    Collects static.
    """
    server.project.collectstatic()


@api.task
def makemessages():
    """
    Generates translations.
    """
    server.project.makemessages()


@api.task
def update_project():
    """
    Loads last project's updates:
        pulls from repository
        makes migrations
        collects static
        generates translations
        restarts project services
    """
    server.project.update()
    server.project.restart()

    if hasattr(server, 'celery'):
        server.celery.restart()

    if hasattr(server, 'celery_beat'):
        server.celery_beat.restart()

    if hasattr(server, 'centrifugo'):
        server.centrifugo.restart()

    if hasattr(server, 'imgproxy'):
        server.imgproxy.restart()

    server.proxy.restart()


@api.task
def configure(service: str=None):
    """
    Configures services.
    If no service provided - installs all project services.

    """
    if not service:
        server.install()
        return

    service = server.get_service(service)

    if not service:
        raise AttributeError

    service.install()
    service.configure()


@api.task
def restart(service: str=None):
    """
    Restarts service.
    If no service provided - restarts all project services.
    Uses soft reset if such option is implemented for service
    """
    service = server.get_service(service)

    if not service:
        raise AttributeError(f'No service `{service}`')

    service.soft_restart()


@api.task
def restart_server(service=None):
    """
    Restarts server
    """
    api.sudo('reboot')


@api.task
def status(service: str) -> str:
    """
    Returns current status for each service.

    Args:
        service (str): Service name.

    Raises:
        AttributeError: Error with service indentifing.

    """
    service = server.get_service(service)

    if not service:
        raise AttributeError(f'No service `{service}`')

    service.status()
