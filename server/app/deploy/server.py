import io, os
from typing import Any
from deploy.path import Path

from fabric import api, contrib, operations


__all__ = (
    'Server',
    'FileSystem',
)


class FileSystem():
    """
    Interface to work with file system.
    """

    def path_exists(self, path: Path) -> bool:
        """
        Returns whether the given path exists on the server.

        Args:
            path (Path): Path on remote server.

        Returns:
            bool: Whether the given path exists or not.
        """
        return contrib.files.exists(path)

    def path_prepared(self, path: Path) -> bool:
        """
        Checkes path on the remote server and processes if it exists
        or not

        Args:
            path (Path): Path on the remote server.

        Returns:
            bool: Whether the given path on remote is available.
        """
        if self.path_exists(path):
            to_remove = operations.prompt(
                f'{path} already exists. Remove it?',
                default='Y'
            )

            if to_remove.lower() == 'y' or not to_remove:
                api.sudo(f'rm -rf {path}')
            else:
                return False

        return True

    def create_directory(self, path: Path, username: str, perms: int=770):
        """
        Creates directory with specified owner and permissions.

        Args:
            path (Path): Path on the remote server.
            username (str): Owner of the directory.
            perms (int, optional): Permissions to grant on directory.

        Returns:
            None
        """
        if not self.path_prepared(path):
            return

        api.sudo(f'mkdir {path}')
        self.set_permissions(username, path, perms)

    def copy(self, data: str, name: str, dest: Path):
        """
        Copies data to file on remote server.

        Args:
            data (str): Data to copy on the file.
            name (str): Name of the file.
            dest (Path): Path to the file on the remote server.
        """
        file = io.StringIO(str(data))
        file.name = name
        file.filename = name

        api.sudo(f'rm -rf {dest}/{name}')
        api.put(file, f'{dest}/{name}', use_sudo=True)

    def set_permissions(self, username: str, path: Path, perms: int=777):
        """
        Sets permissions on specified path.

        Args:
            username (str): System user to grant ownership.
            path (Path): Path on the remote server.
            perms (int, optional): Permissions code to grand.
                Default - 777
        """
        api.sudo(f'chmod -R {perms} {path}'.replace('\\', '/'))
        api.sudo(f'chown -R {username}:{username} {path}'.replace('\\', '/'))


class Server(FileSystem):
    """
    Interface to store common information about server and process
    server like operations
    """
    def __init__(self, services):
        self.services = services
        self.process_services()

    @property
    def _project_alias(self):
        return os.environ['DEPLOY_PROJECT_ALIAS']

    def _aliased_name(self, name: str):
        return f'{self._project_alias}_{name}'

    def process_services(self):
        """
        Saves given services to the server instance.
        """
        self.services = [service(server=self) for service in self.services]
        self._services = {x.name: x for x in self.services}

        for service in self.services:
            # add such things as self.database, self.wsgi etc
            setattr(self, service.type, service)

    def create_user(self, username: str):
        """
        Creates system user.

        Args:
            username (str): Username of user to create.
        """
        with api.settings(warn_only=True):
            result = api.sudo(f'id -u {username}')

            if result.return_code != 1:
                print(f'The user {username} already exists')
            else:
                api.sudo(f'adduser --disabled-password {username}')

    def enable_service(self, name: str):
        """
        Service enabling process. Handles such things as:
            reloading config
            enabling service
            starting service

        Args:
            name (str): System name of the service. E.g. nginx, gunicorn
        """
        api.sudo('systemctl daemon-reload')
        api.sudo(f'systemctl enable {name}.service')

        with api.settings(warn_only=True):
            api.sudo(f'systemctl restart {name}.service')

    def get_service(self, service_name: str) -> Any:
        """
        Returns service to class for direct operations on it.

        Args:
            service_name (str): Name of the service to get

        Returns:
            Any: Service instance.
        """
        return self._services.get(service_name)

    def preconfigure(self):
        """
        Prepars service for django.
        Work on such things like:
            installing locales
            updating packages
            installing common packages needed for default configuration.
        """
        # Install locales
        api.sudo('locale-gen en_US')
        api.sudo('locale-gen en_US.UTF-8')
        api.sudo('update-locale')

        # Install common libraries and programs
        api.sudo('apt-get update')
        api.sudo('''
            apt-get install htop git \
                gettext libtiff5-dev libjpeg8-dev zlib1g-dev \
                libfreetype6-dev liblcms2-dev libwebp-dev libpq-dev \
                build-essential libssl-dev libffi-dev tcl python3-distutils
        ''')

    def install_services(self):
        """
        Installs all the services defined on server.
        Prints statuses of each on them after installation.
        """
        for service in self.services:
            service.install()
            service.configure()

        for service in self.services:
            service.status()

    def restart_services(self):
        """
        Restarts all the services defined on the server.
        """
        with api.settings(warn_only=True):
            for service in self.services:
                    service.restart()

    def check(self):
        if not self.project.user:
            raise AttributeError(
                'Set up separate project user in '
                '`app/deploy/services/project.py`'
            )

    def install(self):
        """
        Prepares server to work on a new django project
        with default configuration.
        """

        self.check()

        # Server preparation.
        self.preconfigure()

        # Installing services
        self.install_services()

        # After installation actions
        self.restart_services()
