from fabric import api

from .base import Service


__all__ = (
    'WSGIService',
)


class WSGIService(Service):
    name = 'gunicorn'
    type = 'wsgi'
    is_global_service = False

    def install(self):
        with self.server.project.source_virtualenv():
            api.run(f'pip install gunicorn gevent')

    def soft_restart(self):
        api.sudo(f'service {self.alias} reload')

    def configure(self):
        """
        Configures service.
        """
        service_settings = self._render_config(
            'blueprints/conf/gunicorn.py',
            PROJECT_NAME=self.alias
        )
        self.server.create_directory(
            f"{self.server.project.root_path / 'confs'}",
            self.server.project.user
        )
        # self.server.create_directory(
        #     f"'/run/{self.alias}'",
        #     self.server.project.user
        # )

        # self.server.create_directory(
        #     f"'/var/log/{self.alias}'",
        #     self.server.project.user
        # )

        self.server.copy(
            service_settings,
            'gunicorn.py',
            self.server.project.root_path / 'confs'
        )

        config = self._render_config(
            'blueprints/conf/gunicorn.service',
            PROJECT_NAME=self.alias,
            PATH_TO_PROJECT=self.server.project.working_dir_path,
            PATH_TO_ENV=self.server.project.env_path,
            PATH_TO_SOCKETS=f'/run/{self.alias}',
            PATH_TO_SETTINGS=self.server.project.root_path / 'confs',
            PROJECT=self.server.project.wsgi_name,
            USER=self.server.project.user,
            ALIAS=self.alias
        )
        self.server.copy(
            config, f'{self.alias}.service', '/etc/systemd/system'
        )

        self.server.enable_service(self.alias)
