import os
from fabric import api


from .base import Service


__all__ = (
    'CentrifugoService',
)


class CentrifugoService(Service):

    name = 'centrifugo'
    type = 'centrifugo'
    is_global_service = False

    #Variables
    centrifugo_port = "8444" #os.environ.get('CENTRIFUGO_PORT', '8444')
    centrifugo_host = "localhost"#os.environ.get('CENTRIFUGO_HOST', 'localhost')
    centrifugo_api_key = os.environ.get('CENTRIFUGO_API_KEY', 'CHANGE_ME')
    centrifugo_hmac_key = os.environ.get('CENTRIFUGO_HMAC_KEY', 'CHANGE_ME')

    def install(self):
        api.sudo('wget https://github.com/centrifugal/centrifugo/releases/download/v2.8.5/centrifugo_2.8.5_linux_amd64.tar.gz')
        api.sudo('tar -xvzf centrifugo_2.8.5_linux_amd64.tar.gz')
        api.sudo('rm centrifugo_2.8.5_linux_amd64.tar.gz CHANGELOG.md LICENSE README.md')
        api.sudo('mv centrifugo /usr/bin')
        api.sudo("""echo \'{
            "token_hmac_secret_key": "%s",
            "admin_password": "admin",
            "admin_secret": "CHANGE",
            "api_key": "%s"
            }\' > /home/bestev/bestev/bestev/centrifugo.json
            """ % (
                self.centrifugo_hmac_key, 
                self.centrifugo_api_key
            )
        )

    def configure(self):
        """
        Configures service.
        """

        config = self._render_config(
            'blueprints/conf/centrifugo.service',
            PATH_TO_CENTRIFUGO_ENV=self.server.project.root_path / 'confs',
            PATH_TO_PROJECT_DIR=self.server.project.dir_path,
            CENTRIFUGO_PORT=self.centrifugo_port,
            CENTRIFUGO_HOST=self.centrifugo_host,
            ALIAS=self.alias,
        )
        print('CENTRIFUGO SERVICE')
        print(config)
        self.server.copy(
            config,
            f'{self.alias}.service',
            '/etc/systemd/system'
        )

        config = self._render_config(
            'blueprints/conf/centrifugo_env',
            CENTRIFUGO_PORT=self.centrifugo_port,
            CENTRIFUGO_HOST=self.centrifugo_host,
            CENTRIFUGO_API_KEY=self.centrifugo_api_key,
            CENTRIFUGO_HMAC_KEY=self.centrifugo_hmac_key,
            ALIAS=self.alias,
        )
        print('CENTRIFUGO ENV')
        print(config)
        self.server.copy(
            config,
            'centrifugo_env',
            self.server.project.root_path / 'confs'
        )

        self.server.enable_service(self.alias)
