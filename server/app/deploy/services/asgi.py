from fabric import api

from .base import Service


__all__ = (
    'ASGIService',
)


class ASGIService(Service):
    """
    Service for caching. Implements here redis-server variant.
    """
    name = 'asgi'
    type = 'asgi'
    is_global_service = False
    base_url = '/ws'

    @property
    def sockets_path(self):
        return f'/run/{self.alias}'

    def install(self):
        with self.server.project.source_virtualenv():
            api.run(f'pip install daphne')

    def soft_restart(self):
        api.sudo(f'service {self.alias} reload')

    def configure(self):
        """
        Configures service.
        """

        config = self._render_config(
            'blueprints/conf/asgi.service',
            PROJECT_NAME=self.server.project.project_name,
            PATH_TO_PROJECT=self.server.project.working_dir_path,
            PATH_TO_ENV=self.server.project.env_path,
            PATH_TO_SOCKETS=self.sockets_path,
            USER=self.server.project.user,
            ALIAS=self.alias,
            PROJECT=self.server.project.wsgi_name
        )
        self.server.copy(
            config, f'{self.alias}.service', '/etc/systemd/system'
        )

        self.server.enable_service(self.alias)
