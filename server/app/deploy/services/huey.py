from fabric import api

from .base import Service


__all__ = (
    'HueyService',
)


class HueyService(Service):
    name = 'huey'
    type = 'huey'
    is_global_service = False

    def install(self):
        with self.server.project.source_virtualenv():
            api.run('pip install huey')
            api.run('pip install gevent')

    def configure(self):
        """
        Configures service.
        """

        config = self._render_config(
            'blueprints/conf/huey.service',
            PATH_TO_HUEY_ENV=self.server.project.root_path / 'confs',
            PATH_TO_PROJECT_ROOT=self.server.project.working_dir_path,
            USER=self.server.project.user,
            ALIAS=self.alias,
        )

        self.server.copy(
            config,
            f'{self.alias}.service',
            '/etc/systemd/system'
        )

        config = self._render_config(
            'blueprints/conf/huey_env',
            PATH_TO_ENV=self.server.project.env_path,
            PROJECT_APP=self.server.project.wsgi_name,
            PROJECT_NAME=self.server.project.name,
            PROJECT_ROOT=self.server.project.working_dir_path,
            ALIAS=self.alias,
        )
        self.server.copy(
            config,
            'huey_env',
            self.server.project.root_path / 'confs'
        )

        self.server.enable_service(self.alias)
