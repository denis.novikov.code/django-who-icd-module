import os

from fabric import api


from .base import Service


__all__ = (
    'ImgproxyService',
)


class ImgproxyService(Service):

    name = 'imgproxy'
    type = 'imgproxy'
    as_subdomain = True
    is_global_service = False

    key = os.environ['IMGPROXY_KEY']
    salt = os.environ['IMGPROXY_SALT']
    signature_size = os.environ['IMGPROXY_SIGNATURE_SIZE']
    exposed_port = os.environ['IMGPROXY_EXPOSED_PORT']
    port = os.environ['IMGPROXY_PORT']
    debug = 'false'
    strip_metadata = os.environ['IMGPROXY_STRIP_METADATA']
    auto_rotate = os.environ['IMGPROXY_AUTO_ROTATE']
    filesystem_root = os.environ['IMGPROXY_LOCAL_FILESYSTEM_ROOT']
    web_compression = os.environ['IMGPROXY_WEBP_COMPRESSION']

    def install(self):
        api.sudo('sudo apt install apt-transport-https ca-certificates curl software-properties-common')
        api.sudo('curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -')
        api.sudo('sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"')
        api.sudo('apt-cache policy docker-ce')
        api.sudo('sudo apt install docker-ce')

        api.sudo('docker pull darthsim/imgproxy:latest')

    def restart(self):
        api.sudo(f'docker stop {self.name}', warn_only=True)
        api.sudo('docker container prune -f')

        api.sudo(f"docker run  -p 48210:8080 -it -d --name {self.name} --restart unless-stopped "
                 f"-v {self.server.project.working_dir_path}/app/uploads:/data/local/uploads "
                 f"-v {self.server.project.working_dir_path}/markup/static:/data/local/static "
                 f"-e IMGPROXY_KEY={self.key} "
                 f"-e IMGPROXY_SALT={self.salt} "
                 f"-e IMGPROXY_SIGNATURE_SIZE={self.signature_size} "
                 f"-e IMGPROXY_EXPOSED_PORT={self.exposed_port} "
                 f"-e IMGPROXY_PORT={self.port} "
                 f"-e IMGPROXY_DEBUG={self.debug} "
                 f"-e IMGPROXY_STRIP_METADATA={self.strip_metadata} "
                 f"-e IMGPROXY_AUTO_ROTATE=={self.auto_rotate} "
                 f"-e IMGPROXY_LOCAL_FILESYSTEM_ROOT={self.filesystem_root} "
                 f"-e IMGPROXY_READ_TIMEOUT=20 "
                 f"-e IMGPROXY_WRITE_TIMEOUT=20 "
                 f"-e IMGPROXY_KEEP_ALIVE_TIMEOUT=20 "
                 f"-e IMGPROXY_CLIENT_KEEP_ALIVE_TIMEOUT=20 "
                 f"-e IMGPROXY_DOWNLOAD_TIMEOUT=10 "
                 "darthsim/imgproxy"
        )

