from .base import Service


__all__ = (
    'CacheService',
)


class CacheService(Service):
    """
    Service for caching. Implements here redis-server variant.
    """
    name = 'redis-server'
    type = 'cache'
    is_global_service = True
