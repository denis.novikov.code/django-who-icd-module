from fabric import api

from .base import Service


__all__ = (
    'ProxyService',
)


class ProxyService(Service):
    """
    Service for proxy server. Nxinx is implemented
    """
    name = 'nginx'
    type = 'proxy'
    is_global_service = True

    def soft_restart(self):
        api.sudo('service nginx reload')

    def remove(self):
        """
        Removes program.
        """
        api.sudo('apt-get remove nginx nginx-common')

    def purge(self):
        """
        Removes program and files.
        """
        api.sudo('apt-get purge --auto-remove nginx nginx-common')

    def configure(self):
        """
        Configures proxy service.
        """

        # self.server.create_user(self.server.project.user)
        # self.server.create_directory(
        #     f'/run/{self.alias}',
        #     self.server.project.user
        # )

        has_asgi = hasattr(self.server, 'asgi')
        has_centrifugo = hasattr(self.server, 'centrifugo')
        has_imgproxy = hasattr(self.server, 'imgproxy')
        imgproxy_as_subdomain = False if not has_imgproxy else self.server.imgproxy.as_subdomain

        conf = self._render_config(
            'blueprints/conf/server_nginx',
            PROJECT_NAME=self.server.project.alias,
            DJANGO_SOCKETS_PATH=f'/run/{self.server.project.alias}',
            PATH_TO_UPLOADS=self.server.project.uploads_path,
            PATH_TO_STATIC=self.server.project.static_path,
            PORT=self.server.project.project_port,
            HAS_ASGI=has_asgi,
            HAS_CENTRIFUGO=has_centrifugo,
            HAS_IMGPROXY=imgproxy_as_subdomain,
            IMGPROXY_AS_SUBDOMAIN=imgproxy_as_subdomain,
            CENTRIFUGO_PORT=self.server.centrifugo.centrifugo_port if has_centrifugo else None,
            CENTRIFUGO_HOST=self.server.centrifugo.centrifugo_host if has_centrifugo else None,
            ASGI_SOCKET=self.server.asgi.sockets_path if has_asgi else None,
            ASGI_BASE_URL=self.server.asgi.base_url if has_asgi else None,
        )
        alias = self.server._aliased_name('nginx')

        with api.settings(warn_only=True):
            api.sudo(f'rm /etc/nginx/sites-enabled/{alias}')

        # Remove default nginx config.
        with api.settings(warn_only=True):
            api.sudo(f'rm /etc/nginx/sites-enabled/default')

        self.server.copy(
            conf,
            alias,
            '/etc/nginx/sites-available'
        )

        with api.settings(warn_only=True):
            api.sudo(
                f'ln -s '
                f'/etc/nginx/sites-available/{alias} '
                '/etc/nginx/sites-enabled/'
            )
