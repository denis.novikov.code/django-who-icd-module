import time
import os
import binascii
from contextlib import contextmanager

from fabric import api, contrib
from deploy.services.base import Service
from deploy.path import Path


__all__ = (
    'ProjectService',
)


class ProjectService(Service):
    """
    Interface to work with on the project on the remote server.
    Handles such things as installing project from repository,
    updating it, etc

    Attributes:
        branch (str): Git branch to pull.
        db_name (str): Database name.
        db_password (str): Database password.
        name (str): Project's service name.
        project_name (str): Project name
        python (str): Python's service name.
        python_version (str): Version of the python.
        repository (str): Repository url.
        server (object): Server instance.
        user (str): System username.
        wsgi_name (str): Name of the WSGI app.
    """
    # System preferences
    # Your project user. Don't call it django. Use your imagination.
    user = os.environ['DEPLOY_PROJECT_ALIAS']

    # Project preferences
    _project_name = os.environ['DEPLOY_PROJECT_ALIAS']

    # Repository preferences
    # Use ssh connection not https
    repository = os.environ['PROJECT_REPOSITORY']
    branch = os.environ['DEPLOY_BRANCH']

    # Python preferences
    python = 'python3.11'
    # Leave version blank for 2.7
    python_version = '3.11'
    # Project ip address/domain name that will be used in ALLOWED_HOSTS
    project_address = os.environ['DEPLOY_IP']
    # Port to work on. If multiple projects on the same server - change it
    project_port = 80

    # Non editable in most cases.
    name = 'gunicorn'  # Name of the WSGI service
    type = 'project'
    wsgi_name = 'app'
    is_global_service = False

    def __init__(self, *args, **kwargs):
        """
        Initialize project's preferences, such as
        random database password, database name, project's name.
        """

        super().__init__(*args, **kwargs)

        self.project_name = self._project_name.lower()
        # self.ssh_path = f'/root/.ssh/{self.server._project_alias}_rsa'
        self.ssh_path = f'/root/.ssh/{self.server._project_alias}_ed25519'

    @property
    def ssh_repository_url(self) -> str:
        user, path = self.repository.split('@')

        return f'{user}@{self.server._project_alias}.{path}'

    @property
    def user_dir_path(self) -> Path:
        """
        Returns home directory for project's user.

        Returns:
            Path: Path to home directory.
        """
        return Path(f'/home/{self.user}')

    @property
    def env_path(self) -> Path:
        """
        Returns path to the environment directory.

        Returns:
            Path: Path to project's environment directory.
        """
        return self.working_dir_path / '.venv'

    @property
    def root_path(self) -> Path:
        """
        Returns path to the project's directory.

        Returns:
            Path: Path to project directory.
        """
        return self.user_dir_path / self.project_name

    @property
    def dir_path(self) -> Path:
        """
        Returns path where the project from repository places.

        Returns:
            Path: Path to the project.
        """
        return self.root_path / self._project_name

    @property
    def working_dir_path(self) -> Path:
        """
        Returns path to the project's `server` directory.

        Returns:
            Path: Path to the server directory.
        """
        return self.dir_path / 'server'

    @property
    def static_path(self) -> Path:
        """
        Returns path where static is collected.

        Returns:
            Path: Path to the collected static.
        """
        return self.working_dir_path / 'app' / 'static'

    @property
    def uploads_path(self) -> Path:
        """
        Returns path where users uploads stuff stores.

        Returns:
            Path: Path to users uploads.
        """
        return self.working_dir_path / 'app' / 'uploads'

    @contextmanager
    def source_virtualenv(self):
        """
        Activates virtualenv as a contextmanager.

        Yields:
            None
        """
        print(self.working_dir_path, '@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        with api.prefix(f'source {self.working_dir_path}/.venv/bin/activate'):
            yield

    def install(self):
        """
        Installs project with all configuration steps.
        """
        # Create system stuff - directpries, users.
        self.server.create_user(self.user)
        self.create_user_directory()
        self.create_project_directory()

        # Install environment.
        self.install_python()

        # Clones project.
        self.clone()

        self.install_env()
        self.update_firewall()

    def configure(self):
        """
        Configures project to work with.
        """
        # Create database.
        self.server.database.create_db()

        self.gen_local_settings()
        self.gen_env()
        self.configure_update()
        self.server.set_permissions(self.user, self.root_path, 775)
        self.create_superuser()
        self.disable_git_permissions_checking()

    def update_firewall(self):
        # 80 port is already open on most servers
        if self.project_port == 80:
            return

        api.run(f'ufw allow {self.project_port}')

    def disable_git_permissions_checking(self):

        api.run(
            f'git config --global --add safe.directory {self.dir_path}'
        )

        api.run(
            f'cd {self.dir_path} && git config core.fileMode false'
        )

    def create_superuser(self):
        with self.source_virtualenv():
            api.sudo(
                f'python {self.working_dir_path}/manage.py createsuperuser'
            )

    def gen_local_settings(self):
        """
        Generates local settings and copies it to the remote.
        """
        local_settings = self._render_config(
            'blueprints/project/local.py',
            ALLOWED_HOSTS=self.project_address,
        )

        # Copy settings file
        self.server.copy(
            local_settings,
            'local.py',
            self.working_dir_path / 'app' / 'settings'
        )

    def gen_env(self):
        """
        Generates env file and copies it to the remote.
        """
        has_centrifugo = hasattr(self.server, 'centrifugo')

        env_file = self._render_config(
            'blueprints/project/local.env',
            SECRET_KEY=binascii.hexlify(os.urandom(24)).decode(),
            ALLOWED_HOSTS=self.project_address,
            DB_NAME=self.server.database.db_name,
            DB_USER=self.server.database.db_user,
            DB_PASSWORD=self.server.database.db_password,
            HAS_CENTRIFUGO=has_centrifugo,
            CENTRIFUGO_PORT=self.server.centrifugo.centrifugo_port if has_centrifugo else None,
            CENTRIFUGO_HOST=self.server.centrifugo.centrifugo_host if has_centrifugo else None,
            CENTRIFUGO_API_KEY=self.server.centrifugo.centrifugo_api_key if has_centrifugo else None,
            CENTRIFUGO_HMAC_KEY=self.server.centrifugo.centrifugo_hmac_key if has_centrifugo else None,
        )

        # Copy settings file
        self.server.copy(
            env_file,
            '.env',
            self.working_dir_path
        )

    def generate_ssh_config(self):
        api.run('touch ~/.ssh/config')

        # GitLab only support PubkeyAuthentication not RSAAuthentication
        contrib.files.append(
            '~/.ssh/config',

            f'Host {self.server._project_alias}.gitlab.com\n'
            '    HostName gitlab.com\n'
            '    PubkeyAuthentication yes\n'
            f'    IdentityFile {self.ssh_path}\n'
            '    IdentitiesOnly yes\n'
            '    User git\n'
        )

    def clone(self):
        """
        Clones project from repository
        """
        print('Installing ssh key for your repository.')
        time.sleep(2)

        if not self.server.path_exists(self.ssh_path):
            # api.run(f'ssh-keygen -t rsa -b 4096 -f {self.ssh_path}')
            api.run(f'ssh-keygen -t ed25519 -f {self.ssh_path}')

        api.run(f'cat {self.ssh_path}.pub')
        api.prompt('Press enter when you\'ve installed ssh key to repository')

        if not self.server.path_prepared(self.dir_path):
            return

        self.generate_ssh_config()

        api.run(
            f'git clone -b {self.branch} {self.ssh_repository_url} {self.dir_path}',
            # user=self.user
        )

    def create_user_directory(self):
        """
        Creates user directory.
        """
        self.server.create_directory(self.user_dir_path, self.user)

    def create_project_directory(self):
        """
        Creates project directory.
        """
        self.server.create_directory(self.root_path, self.user)

    def pull(self):
        """
        Pulls last project updates from repository.
        """
        api.sudo(f'git -C {self.dir_path} pull origin {self.branch}')

    def update(self):
        """
        Updates project data. Do such things like:
            update requirements
            make migrations
            make messages
            collect static
        """
        self.pull()
        self.install_requirements()
        self.migrate()
        self.jsmakemessages()
        self.collectstatic()
        # self.create_postie_templates()
        # self.update_user_permissions()

    def configure_update(self):
        """
        Litle bit changed update() with unique for first configure jobs
        """
        self.pull()
        self.install_requirements()
        self.migrate()
        self.makemessages()
        self.jsmakemessages()
        self.collectstatic()
        # self.create_postie_templates()

    def install_python(self):
        """
        Installs python version.
        """

        # Enable it if you have old OS
        # api.sudo('add-apt-repository ppa:fkrull/deadsnakes')
        api.sudo('apt-get update')

        api.sudo(f'sudo apt install software-properties-common')
        api.sudo(f'sudo add-apt-repository ppa:deadsnakes/ppa')
        api.sudo(f'apt-get install {self.python}')
        api.sudo(f'apt-get install {self.python}-distutils')

        api.sudo(f'PYTHON_VERSION="{self.python_version}"')
        api.sudo(f'sudo apt install {self.python}-dev')

    def install_env(self):
        """
        Installs python environment.
        """
        if not self.server.path_exists(self.env_path):
            api.sudo('apt-get install python3-pip')
            api.sudo('pip3 install pipenv')
            api.sudo(f'export PIPENV_VENV_IN_PROJECT=True && cd {self.working_dir_path} && pipenv --python {self.python_version}')
            # api.sudo('apt-get install python-pip')
            # api.run('pip install virtualenv')
            # api.run(f'virtualenv -p {self.python} {self.env_path}')

    def install_requirements(self):
        """
        Updates requirements.
        """
        # pass
        with self.source_virtualenv():
            api.run(f'cd {self.working_dir_path} && pipenv sync')
            # api.run(f'cd {self.working_dir_path} && pipenv install')
            # api.run(f'pip install -r {self.working_dir_path}/requirements.txt')

    def migrate(self):
        """
        Makes migrations.
        """
        # hack for mysql
        # api.sudo('service mysql restart')
        with self.source_virtualenv():
            api.run(f'python {self.working_dir_path}/manage.py migrate')

    def makemessages(self):
        """
        Generates messages.
        """
        with self.source_virtualenv():

            api.run(
                f'python {self.working_dir_path}/manage.py makemessages -l ru -l en -l fr -e py,html,jinja'
            )

        self.server.set_permissions(self.user, self.user_dir_path, 755)

    def jsmakemessages(self):
        """
        Update messages.
        """
        with self.source_virtualenv():

            api.run(
                f'cd {self.dir_path} && '
                f'python server/manage.py jsmakemessages -l ru -l en -l fr -k t -k tc -v3 -e jinja,py,html,js,ts,jsx,tsx,vue -jse js,ts,jsx,tsx,vue -i node_modules -i "*~" -i "*.pyc" -i "CVS" -i ".venv" --no-default-ignore --symlinks'
            )

        self.server.set_permissions(self.user, self.user_dir_path, 755)

    def collectstatic(self):
        """
        Collects static.
        """
        with self.source_virtualenv():
            api.run(
                f'python {self.working_dir_path}/manage.py collectstatic '
                '--noinput'
            )

        self.server.set_permissions(self.user, self.user_dir_path, 755)

    def create_postie_templates(self):
        with self.source_virtualenv():
            api.run(f'python {self.working_dir_path}/manage.py create_templates')

    def run_centrifugo(self):
        api.sudo(f'cd {self.dir_path} && docker-compose -f docker-compose.prod.yml up -d')

    def restart(self):
        api.sudo(f'service {self.alias} restart')

    def update_user_permissions(self):
        with self.source_virtualenv():
            api.run(f'python {self.working_dir_path}/manage.py update_user_permissions')
