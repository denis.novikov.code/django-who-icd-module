from fabric import api
from typing import TYPE_CHECKING
import jinja2

from deploy.path import Path

if TYPE_CHECKING:
    from deploy.server import Server


__all__ = (
    'Service'
)


class Service():
    """
    Interface to work with the services on the server.

    Attributes:
        name (str): name of the service on server
        user (str): service user(who runs the service).
            If no user - run as sudo
    """

    name = ''
    blueprints_path = ''
    type = ''
    is_global_service = True

    def __init__(self, server: 'Server'):
        self.server = server

        if not self.is_global_service:
            self.alias = self.server._aliased_name(self.name)
        else:
            self.alias = self.name

    @property
    def is_installed(self) -> bool:
        """
        Returns whether the service is installed on the remote server.

        Returns:
            bool: Whether the service is installed.
        """
        with api.settings(warn_only=True):
            if api.sudo(f'service --status-all | grep "{self.alias}"'):
                return True

        return False

    def _render_config(self, file_path, **context):
        """
        Renders configs using given context.

        Args:
            file_path (str): path to config in project
            **context: extra arguments passed to the template context
        """
        path = Path(file_path)

        return jinja2.Environment(
            loader=jinja2.FileSystemLoader(str(path.parents[0].absolute()))
        ).get_template(path.name).render(context)

    def install(self):
        """
        Installs service on server.
        """
        api.sudo(f'apt-get install {self.alias}')

    def configure(self):
        """
        Configures service.
        """
        return

    def create(self):
        """
        Installs and sets up service on server
        """
        self.install()
        self.configure()
        self.start()

    def status(self):
        """
        Prints current status of service.
        """
        pass
        # with api.settings(warn_only=True):
        #     api.sudo(f'service {self.name} status')

    def start(self):
        """
        Starts service.
        """
        api.sudo(f'service {self.alias} start')

    def stop(self):
        """
        Stops service.
        """
        api.sudo(f'service {self.alias} stop')

    def remove(self):
        """
        Removes service.
        """
        api.sudo(f'apt-get remove {self.alias}')

    def purge(self):
        """
        Purges service from server(remove it and all configs).
        """
        api.sudo(f'apt-get purge --auto-remove {self.alias}')

    def restart(self):
        """
        Restarts service.
        """
        api.sudo(f'service {self.alias} restart')

    def soft_restart(self):
        """
        Restarts service. No uptime for soft reset(valuable for deploy).
        Must be implemented for each service, so by default - hard reset
        """
        self.restart()
