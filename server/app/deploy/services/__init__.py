from .proxy import *
from .database import *
from .cache import *
from .wsgi import *
from .project import *
from .celery import *
from .celery_beat import *
from .asgi import ASGIService

from .centrifugo import CentrifugoService
# from .imgproxy import ImgproxyService
from .who_icd import WhoIcdService

enabled = [
    ProxyService,
    CacheService,
    DatabaseService,
    ProjectService,
    WSGIService,
    CeleryService,
    CeleryBeatService,
    WhoIcdService,
    # ImgproxyService,
    # CentrifugoService,
    # ASGIService
]
