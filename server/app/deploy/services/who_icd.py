import os

from fabric import api


from .base import Service


__all__ = (
    'WhoIcdService',
)


class WhoIcdService(Service):

    name = 'who_icd'
    type = 'who_icd'
    as_subdomain = True
    is_global_service = False

    languages = os.environ['ICD_AVAILABLE_LANGUAGES']

    def install(self):
        api.sudo('sudo apt install apt-transport-https ca-certificates curl software-properties-common')
        api.sudo('curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -')
        api.sudo('sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"')
        api.sudo('apt-cache policy docker-ce')
        api.sudo('sudo apt install docker-ce')

        api.sudo('docker pull whoicd/icd-api:latest')

    def restart(self):
        api.sudo(f'docker stop {self.name}', warn_only=True)
        api.sudo('docker container prune -f')

        # api.sudo(f"docker run -p 9000:80 -e include=2024-01_ar-cs-en-es-fr-pt-ru-tr-uz-zh -e acceptLicense=true -e saveAnalytics=true whoicd/icd-api")


