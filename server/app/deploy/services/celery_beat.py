from fabric import api

from .base import Service


__all__ = (
    'CeleryBeatService',
)


class CeleryBeatService(Service):
    """
    Service for caching. Implements here redis-server variant.
    """
    name = 'celery_beat'
    type = 'celery_beat'
    is_global_service = False

    def install(self):
        with self.server.project.source_virtualenv():
            api.run(f'pip install celery')

    def configure(self):
        """
        Configures service.
        """
        # self.server.create_directory(
        #     f"'/run/{self.alias}'",
        #     self.server.project.user
        # )
        # self.server.create_directory(
        #     f"'/var/log/{self.alias}'",
        #     self.server.project.user
        # )

        config = self._render_config(
            'blueprints/conf/celery_beat.service',
            PATH_TO_ENV=self.server.project.env_path,
            PATH_TO_CELERY_ENV=self.server.project.root_path / 'confs',
            PATH_TO_PROJECT_ROOT=self.server.project.working_dir_path,
            USER=self.server.project.user,
            ALIAS=self.alias,
        )

        self.server.copy(
            config,
            f'{self.alias}.service',
            '/etc/systemd/system'
        )

        config = self._render_config(
            'blueprints/conf/celery_beat_env',
            PATH_TO_ENV=self.server.project.env_path,
            PROJECT_NAME=self.server.project.name,
            PROJECT_ROOT=self.server.project.working_dir_path,
            ALIAS=self.alias,
            PROJECT_APP=self.server.project.wsgi_name,
        )
        self.server.copy(
            config,
            'celery_beat_env',
            self.server.project.root_path / 'confs'
        )

        self.server.enable_service(self.alias)
