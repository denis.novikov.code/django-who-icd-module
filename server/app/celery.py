import os
import sys

import django

from django.conf import settings
from celery import Celery, schedules
from celery.schedules import crontab

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app.settings')

PATH = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(os.path.dirname(PATH)))
sys.path.append(os.path.dirname(PATH))
sys.path.append(PATH)

app = Celery('app')

# Using a string here means the worker don't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
app.conf.timezone = settings.TIME_ZONE

app.conf.beat_schedule = {
    'continue_parsing': {
        'task': 'apps.who_icd.contrib.parser.tasks.continue_parsing_task',
        'schedule': crontab(minute='*/5'),
    },
}

if __name__ == "__main__":
    django.setup()